# FHIR2Milou service

| Summary                          |                                                                                                                     |
|----------------------------------|---------------------------------------------------------------------------------------------------------------------|
| Service name (computer friendly) | fhir2milou-service                                                                                                        |
| Maturity level                   | Concept [ ] Prototype [ ] Demonstration [x] Product [ ]                                                                                                         |
| Used externally                  | No                                                                   |
| Database                         | No                                                                                                                  |
| RESTful dependencies             | outcomedefinition-service, patientcare-service   |
| Messaging dependencies           | Kafka                                                                                                               |
| Other dependencies               | In DIAS configuration: User context service (user context info from session ID)                                                            |
| License                          | Apache 2.0                                                                                                          |

The fhir2milou-service is a microservice intented for use in a microservice based system as the one described at 
[GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md). The service is a 
_stateless computation services_ as described in [GMK Backend Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/HFS.md). 
The service processes FHIR Bundles containing CTG data and sends the extracted data to a 
[Medexa Milou](http://www.medexa.se/en/milouenglish/) server 

## Documentation

  - [GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md)
  - Design documentation, FHIR API and messaging API documentation: See [docs](docs//) folder.
  - Use of these FHIR resources and how they relate to other resources in the system on the pages about the [FHIR data model](https://issuetracker4s.atlassian.net/wiki/spaces/FDM/overview) 
  - Public classes and methods are documented with godoc 
  - More on [DIAS](https://www.rm.dk/om-os/digitalisering/digitale-losninger-pa-region-midtjyllands-udviklingsplatform/).

## Prerequisites
To build the project you need:

  * OpenJDK 11+
  * Maven 3.1.1+
  * Docker 20+

The project depends on git submodule (ms-scripts). To initialize this you need to execute:

```
git submodule update --init
git submodule foreach git pull origin master
``` 

If you are running things using docker-compose (see below) you also need to have a docker network by the name of 
`opentele3net` setup:

`docker network create opentele3net` 

## Building the service
The simplest way to build the service is to use the `buildall.sh` script:

`/scripts/buildall.sh`

This will build the service as a WAR file and add this to a Jetty based docker image.

## Running the service
You can run the service just using ``docker run ...`` or you can run the service using the provided docker-compose files. 
You may of course also run the service in container-orchestration environments like Kubernetes or Docker Swarm.

### Running the service with docker-compose
With docker-compose you can run the service like this:

`docker-compose up`

## Logging
The service uses Logback for logging. At runtime Logback log levels can be configured via environment variables (see
below). 

_Notice:_ The service may output person sensitive information at log level `TRACE`. If log level is set to `DEBUG` or 
above no person sensitive information should be output from the service.

If you are running via docker-compose and you want use the fluentd logging driver (if you for instance have setup 
fluentd, elasticsearch and Kibana) instead of the standard json-file you may start the service like this:

```
docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml up
```

## Monitoring
With the following environment variables you may enable that instances of the KafkaProcessAndConsume class touch a file at specified intervals:

ENABLE_HEALTH=true
HEALTH_FILE_PATH=/tmp/health
HEALTH_INTERVAL_MS=5000

This can be used to check the health of the KafkaProcessAndConsume thread - typically by having a shell script check on 
the timestamp of the health file.

## Environment variables

The files `service.env` and `deploy.env` define all environment variables used by this microservice. `deploy.env` defines
service URLs and log levels that depend on your particular deployment.


| Environment variable          | Required | Description                                                                                                                     |
|-------------------------------|----------|----------------------------------------------------------------------
| SERVICE_NAME                  | y | Computer friendly name of this service. 
| FHIR_VERSION                  | y | Version of HL7 FHIR supported by this service
| PATIENTCARE_SERVICE_URL           | n | Required if the MOCK_PATIENT_RESOLVER is set to false. The full address of the service where patients can be looked up
| OUTCOME_SERVICE_URL            | n | Required if the MOCK_DEVICE_RESOLVER is set to false. The full address of the service where devices can be looked up
| CLEAR_CACHE_TIME_OUT          | y | The time in milliseconds before a device or patient resource is cleared from the cache
| *Systems and extensions:*     | |
| PATIENT_IDENTIFIER_SYSTEM     | y | The official identifier system profiled for use in your FHIR patient resources. In Denmark this must be the CPR system (urn:oid:1.2.208.176.1.2)
| REGISTRATION_ID_EXTENSION_URL | y | In a FHIR bundle with CTG data, the URL of the extension that codes the stream ID identifying a stream of CTG data
| SEQUENCE_NUMBER_EXTENSION_URL | y | In a FHIR bundle with CTG data, the URL of the extension that codes the index of the first item in the valueSampleData array in a CTG recording
| *Mocks and integration testing:*          | |
| MOCK_PATIENT_RESOLVER         | y | If true, the lookup of the patient resource is mocked 
| MOCK_DEVICE_RESOLVER          | y | If true, the lookup of the device resource is mocked 
| MOCK_PATIENT_FIRST_NAME       | n | Required if MOCK_PATIENT_RESOLVER is set to true. The first name of the mocked patient
| MOCK_PATIENT_LAST_NAME        | n | Required if MOCK_PATIENT_RESOLVER is set to true. The last name of the mocked patient
| MOCK_PATIENT_CPR              | n | Required if MOCK_PATIENT_RESOLVER is set to true. The CPR number of the mocked patient
| MOCK_DEVICE_MODEL_NUMBER      | n | Required if MOCK_DEVICE_RESOLVER is set to true. The model number of the mocked device
| INTEGRATION_TEST              | n | Set to true if you wish to run tests without mocking of external dependencies
| *Messaging related:*          | |
| ENABLE_KAFKA                  | y | If true Kafka communication is enabled for this service
| KAFKA_BOOTSTRAP_SERVER        | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_KEY_SERIALIZER          | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_VALUE_SERIALIZER        | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_KEY_DESERIALIZER        | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_VALUE_DESERIALIZER      | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_ENABLE_AUTO_COMMIT      | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_AUTO_COMMIT_INTERVAL_MS | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_SESSION_TIMEOUT_MS      | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_GROUP_ID                | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_ACKS                    | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_RETRIES                 | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| ENABLE_HEALTH                 | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| HEALTH_FILE_PATH              | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| HEALTH_INTERVAL_MS            | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| *Authentication*              | |
| ENABLE_AUTHENTICATION         | n | Enable getting user context information (from http session header or otherwise)
| ENABLE_DIAS_AUTHENTICATION    | n | Enable DIAS specific authentication functionality
| USER_CONTEXT_SERVICE_URL      | n | URL of user context side care service. Required if ENABLE_DIAS_AUTHENTICATION=true
| *Log levels:*                 ||
| LOG_LEVEL                     | y | Set the log level for 3rd party libraries
| LOG_LEVEL_DK_S4               | y | Set the log level for this service and the 4S libraries used by this service

## Test
To do a basic test of the service you may run

```
mvn clean test
```
### Smoketest without authentication
Set `ENABLE_AUTHENTICATION=false` and `ENABLE_DIAS_AUTHENTICATION=false` and run the service like this:

`docker-compose -f docker-compose.yml up`

If you have startet an instance of Kafka on the the same docker network as the service is running on you may
smoketest the messaging part of the service by running the following with kafkacat (You can find kafkacat at 
https://github.com/edenhill/kafkacat.): 

`kafkacat -b kafka:9092 -P -t InputReceived_FHIR_Bundle_MDC8450059 src/test/resources/bundleOfflineConsolidatedKafkaMsg.json`

As a result of this you should a.o. see a message on the log saying `EventConsumer processing: topic = InputReceived_FHIR_Bundle_MDC8450059`.
(If you run the service in a local setup you will also see an  `Error connecting to patient service` due to the local setup
not running a patientcare-service, see [GMK Backend Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/HFS.md))

### Smoketest with authentication
To smoketest the service with [DIAS](https://www.rm.dk/om-os/digitalisering/digitale-losninger-pa-region-midtjyllands-udviklingsplatform/) 
authentication turned on run Set `ENABLE_AUTHENTICATION=true` and `ENABLE_DIAS_AUTHENTICATION=true` and set 
`USER_CONTEXT_SERVICE_URL` to point to a sidecar service with a [getsessiondata](https://telemed-test.rm.dk/leverandorportal/services/sidevogne/getsessiondata/)
endpoint.