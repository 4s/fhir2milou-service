## FHIR2Milou service

The FHIR2Milou Service is a software product designed to be used as a module in a microservice based system capable
of receiving and handling cardiotocography (CTG) recordings. This FHIR2Milou module will enable the system to export CTG 
recordings to a particular external clinical system, namely the MILOU system provided by MEDEXA Diagnostisk Service AB 
(MEDEXA).

The service was designed for use with a remote patient monitoring system targeted at pregnant women (GMK) which offer 
the capability of performing CTG recordings at home; and if the FHIR2Milou module is installed on such a system, 
the CTG recordings can be exported to the MILOU system for storage and evaluation by clinical professionals. Read more about
the GMK project [here](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/)

The service subscribe to HL7 FHIR based resources containing completed online or offline cardiotocography (CTG) recordings
from the message queue. The service validates, converts and processes these recordings and exports them to the MILOU system.

## Intended use

This service is not intended for use with time-critical scenarios, nor does it guarantee successful delivery.
It is up to the user of the service to provide a means of communication when a message has successfully been sent to
the MILOU server. For more on intended use:
https://bitbucket.org/4s/fhir2milou-service/src/5a12148bed6ffbd850315dcb0aafdf4e1026de36/docs/technical_file/general_description.md?at=RiskManagement.

## Design

The service supports offline (batch) and online (real-time/continuous) CTG recordings.

When the service receives a CTG recording it validates that the resource contains the information required by the MILOU server
without any duplicates (see the MILOU server documentation). During the validation step, all required data is extracted 
from the observation into a ValidationResult object. If no validation errors are found, the service performs unit 
conversions on the sampled data values in the observation according to the FHIR standard see https://www.hl7.org/fhir/datatypes.html#sampleddata.
The service then checks to see if the observation is an offline CTG recording (a bundle with its status as final) 
or part of an online CTG recording.

The first part of an online CTG recording is a HL7 FHIR bundle resource with its status set to preliminary. This can be
followed by zero or more HL7 FHIR observation resources with their status set to preliminary. The final part is a a HL7 
FHIR observation resource with its status set to final. For examples on offline and online CTG bundles 
please see here https://bitbucket.org/4s/sample-data/src/master/src/main/resources/FHIR-documented/CTG/.

In either case, the service looks up the patient resource by calling the PatientCare-service see https://bitbucket.alexandra.dk/projects/S4/repos/patientcare-service/browse
 and the PHD device resource by calling the Outcome-service [see https://bitbucket.alexandra.dk/projects/S4/repos/outcome-service/browse] included in the CTG recording.

If the recording is the first part of an online CTG, the patient and device resources are cached under the
online recording registration ID (GUID) for the subsequent parts of the recording. The stop message will remove these resources
from the cache.  A timeout will remove them from the cache in case a stop message is never received.

If the device service is unavailable, FHIR2Milou service will retry a number of times before giving up and returning 
an error on the messaging system see https://resilience4j.readme.io/docs/retry.

For offline CTGs, the service merges the sampled data values and GZIPs them in an encoded string.
The service then sends the recording via a SOAP message to the Milou Server including the extracted data 
from the ValidationResult object and writes a success message to Kafka.

If the FHIR2Milou service rejects the SOAP message, the service writes an error to the Kafka queue on topic ProcessingFailed. 
If the FHIR2Milou service accepts the message, the service will write a success message to the Kafka queue on topic DataValidated.

![alt text](seq.png)
![alt text](uml.png)

## Messaging API
The specification of the Kafka messaging API for this service is described in the file `messagingapi.yml`.

## Missing
- Consider a status endpoint which returns whether a recording was delivered to the MILOU server.

## SOAP format
The SOAP messages are constructed from the provided online.wsdl or offline.wsdl files.