# Design review 
## by Helena Meyer

- service renamed to FHIRObs2Milou service. (DONE)
- corrections to the two diagrams on the page:
  - Milou service -> FHIRObs2Milou service
  - Input service -> Obs-input-service
 -  "The CTG bundle is then validated" ... Add a short description of what exactly is validated here...
 -  "A SOAP message is then sent" ... Add sections abot the alternatives (what happens if validation fails or server is 
     down etc.)
 -  Continuous CTG: write that we do not need to model/implement this in KomTel 2 project...


Corrections to review:
-  Rename Milou service -> FHIR2Milou service
-  Continuous CTG might be a part of the KomTel 2 project
-  Remember error handling !!