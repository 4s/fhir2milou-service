# Design review 
## by Jacob Andersen

- On both diagrams the service has not yet been renamed to "FHIR2Milou service" - on one it is called "FHIRObs2Milou" 
and on the other it is called "Milou service".
- In the message sequence diagram, the arrow "CTG sent to Milou Server" goes from the FHIR2Milou service directly to the 
observation service - not to kafka. This suggests that the operation is synchronous - which it should not be. 
The textual explanation correctly states that the message is sent to kafka, so this is just an issue with the diagram
