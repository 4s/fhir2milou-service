# API documentation
The files in this folder document the external APIs of this service.

## Messaging API
The file [fhir2milou-service.yaml](fhir2milou-service.yaml) documents the asynchronous messaging API in the form of an 
[AsyncAPI specification](https://www.asyncapi.com/).

Notice that an AsyncAPI specification documents the API as seen from the point of view of what a client is allowed to 
do. That is the topics and messages that this service is producing are documented as `subscribe` type channels and the 
topics and messages this service is consuming are documented as ``publish`` type channels.

__Generating html:__ To generate html from the AsyncAPI yaml file use the AsyncAPI Generator. Follow instructions on 
https://www.asyncapi.com/generator to install and run the generator. You may also run the Generator through Docker, f.ex.:

```
docker run --rm -it -v $PWD:/app/ asyncapi/generator -o /app/output /app/services/inprocces/outcomedefinition-service.yaml @asyncapi/html-template --force-write
```