package dk.s4.microservices.milouservice.test;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.google.common.io.Resources;
import dk.s4.microservices.FHIR2MilouService.Exceptions.FatalFormatException;
import dk.s4.microservices.FHIR2MilouService.Exceptions.FatalInternalErrorException;
import dk.s4.microservices.FHIR2MilouService.Utils.*;
import dk.s4.microservices.FHIR2MilouService.processing.MyEventProcessor;
import dk.s4.microservices.FHIR2MilouService.service.FHIR2Milou;
import dk.s4.microservices.messaging.*;
import dk.s4.microservices.messaging.Message.BodyCategory;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.messaging.mock.MockKafkaConsumeAndProcess;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import milouWsdlCode.Registration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.Serializer;
import org.hl7.fhir.r4.model.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.XMLGregorianCalendar;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class OfflineCTGTest {

    private final static Logger ourLog = LoggerFactory.getLogger(OfflineCTGTest.class);

    private static final String observationFileName = "FHIR-clean/CTG/offline_ex_001_pt_consolidated.json";
    private static final String observationFileNameFail = "ctg_bundle_fail.json";

    private static MockProducer<String, String> mockExternalKafkaProducer;
    private static MockKafkaEventProducer mockKafkaEventProducer;

    private static String fhirObsAsString;
    private static String fhirObsAsStringFail;

    private static String ENVIRONMENT_FILE = "service.env";
    private static String KAFKA_PRODUCER_PROPS = "producer.props";
    private static String DEPLOYMENT_FILE = "deploy.env";

    private static Thread mockKafkaConsumeAndProcessThread;
    private static  MockKafkaConsumeAndProcess kafkaConsumeAndProcess;

    private static final int FUTURE_TIMEOUT = 5000;

    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testBundleValidation(){

        FhirContext fhirContext = FhirContext.forR4();
        IParser parser = fhirContext.newJsonParser();
        Bundle parsedBundle = null;

        try {
            parsedBundle = parser.parseResource(Bundle.class, fhirObsAsString);
        } catch (Exception e) {
            fail("Method should not throw exception" + e);
        }

        CTGValidator validator = new CTGValidator();
        ValidationResult evo = null;

        try{
            evo = validator.validate(parsedBundle);
        }
        catch (Exception e){
            fail("Method should not throw exception");
        }

        List<String> validationErrors = evo.getValidationErrors();

        assertEquals(0, validationErrors.size());
        assertEquals( "00-80-98-FE-FF-0E-39-13", evo.getDeviceReferences().get(0).getIdentifier().getValue());

        assertEquals("0201919996", evo.getPatientRef().getIdentifier().getValue());

        Calendar startTime = null;

        try{
            startTime = DateUtil.stringToCalendar("2017-06-12T10:48:05+02:00");
        }
        catch (ParseException e){
            fail("This should not throw an exception");
        }

        assertEquals(evo.getStartTime().compareTo(startTime), 0);

        String data = "323 322 322 322 323 323 322 322 323 324 323 322 322 322 323 323 322 322 323 324 323 322 322 322 323 323 322 322 323 324 323 322 322 322 323 323 322 322 323 324";
        assertEquals(data, evo.getMHR().getData());
        assertEquals(0.0, evo.getMHR().getOrigin().getValue().doubleValue(), 0);
        assertEquals(1, evo.getMHR().getDimensions());
        assertEquals(0.25, evo.getMHR().getFactor().doubleValue(), 0);
        assertEquals(250, evo.getMHR().getPeriod().intValue());

        data = "561 562 562 561 560 561 562 563 562 561 561 562 562 561 560 561 562 563 562 561 561 562 562 561 560 561 562 563 562 561 561 562 562 561 560 561 562 563 562 561";
        assertEquals(data, evo.getFHR().getData());
        assertEquals(0.0, evo.getFHR().getOrigin().getValue().doubleValue(), 0);
        assertEquals(1, evo.getFHR().getDimensions());
        assertEquals(0.25, evo.getFHR().getFactor().doubleValue(), 0);
        assertEquals(250, evo.getFHR().getPeriod().intValue());

        data = "44 41 42 41 41 40 41 43 42 42 40 41 42 41 41 40 42 44 44 43 40 41 42 41 41 40 41 43 42 42 40 41 42 41 41 40 42 44 44 43";
        assertEquals(data, evo.getTOCO().getData());
        assertEquals(0.0, evo.getTOCO().getOrigin().getValue().doubleValue(), 0);
        assertEquals(1, evo.getTOCO().getDimensions());
        assertEquals(1.96, evo.getTOCO().getFactor().doubleValue(), 0);
        assertEquals(250, evo.getTOCO().getPeriod().intValue());


        data = "2 2 2 1 2 2 1 2 2 2 2 1 0 0 1 2 2 2 2 2 2 2 2 2 2 2 2 1 2 2 1 2 2 2 2 1 0 0 1 2";
        assertEquals(data, evo.getSQ().getData());
        assertEquals(0.0, evo.getSQ().getOrigin().getValue().doubleValue(), 0);
        assertEquals(1, evo.getSQ().getDimensions());
        assertEquals(1, evo.getSQ().getFactor().doubleValue(), 0);
        assertEquals(250, evo.getSQ().getPeriod().intValue());

        assertEquals(2, evo.getMarkers().size());

        Calendar marker1 = null;
        Calendar marker2 = null;

        try{
            marker1 = DateUtil.stringToCalendar("2017-06-12T10:48:08+02:00");
            marker2 = DateUtil.stringToCalendar("2017-06-12T10:48:10+02:00");
        }
        catch (ParseException e){
            fail("This should not throw an exception");
        }

        assertEquals(0, evo.getMarkers().get(0).compareTo(marker1));
        assertEquals(0, evo.getMarkers().get(1).compareTo(marker2));

    }

    @Test
    public void testSOAPMessageCreator() throws FatalInternalErrorException, FatalFormatException {

        XMLGregorianCalendar contactTime = null, startTime = null, endTime = null;
        List<XMLGregorianCalendar> markers = new ArrayList<>();

        try{
            contactTime = DateUtil.stringToXMLGregorian("2018-02-03T15:05:10+02:00");
            endTime = DateUtil.stringToXMLGregorian("2018-06-07T18:03:11+02:00");
            startTime = DateUtil.stringToXMLGregorian("2017-01-01T06:01:14+02:00");

            markers.add(DateUtil.stringToXMLGregorian("2017-01-01T08:01:14+02:00"));
            markers.add(DateUtil.stringToXMLGregorian("2017-02-02T08:01:14+02:00"));
            markers.add(DateUtil.stringToXMLGregorian("2017-03-03T08:01:14+02:00"));

        }
        catch (Exception e){
            fail("Method should not throw an error");
        }

        PatientResolver patientRes = FHIR2Milou.getPatientResolver();
        Patient patient = patientRes.getPatient(new Reference());

        byte[] ctgData = null;

        try{
            ctgData = Utility.toGZIP("This is test data");
        }
        catch (Exception e){
            fail("Method should not throw this error: " + e);
        }

        DeviceResolver deviceResolver = FHIR2Milou.getDeviceResolver();
        Device device = deviceResolver.getDevice(new Reference());

        Registration message = SOAPMessageCreator.createOfflineMessage(patient,
                contactTime, endTime, startTime, device, markers, ctgData);


        assertNotNull(message );

        assertNotNull(message.getMarkersField().getDateTime());
        assertEquals(markers.size(), message.getMarkersField().getDateTime().size());

        for (int i = 0; i < markers.size(); i++){
            assertEquals(markers.get(i), message.getMarkersField().getDateTime().get(i));
        }

        assertNotNull(message.getPatientField());
        assertEquals(patient.getName().get(0).getGivenAsSingleString(), message.getPatientField().getNameField().getFirstField());
        assertEquals(patient.getName().get(0).getFamily(), message.getPatientField().getNameField().getLastField());
        assertEquals(patient.getIdentifier().get(0).getValue(), message.getPatientField().getIdField());

        assertNotNull(message.getDeviceIDField());
        assertEquals(device.getModelNumber() + " " + device.getSerialNumber() , message.getDeviceIDField());

        assertNotNull(message.getStartTimeField());
        assertEquals(startTime, message.getStartTimeField());

        assertNotNull(message.getEndTimeField());
        assertEquals(endTime, message.getEndTimeField());

        assertNotNull(message.getContactField());
        assertEquals(contactTime, message.getContactField().getTimeField());

        assertNotNull(message.getTocoShiftField());
        assertEquals(0, message.getTocoShiftField().intValue());

        assertNotNull(message.getCtgField());
        assertEquals(ctgData, message.getCtgField());

    }

    @Test
    public void testSendBundle2Kafka() throws Exception {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);

        Message kafkaMessage = new Message();
        kafkaMessage.setBody(fhirObsAsString);
        kafkaMessage.setSender("TestProducer");
        kafkaMessage.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        kafkaMessage.setTransactionId(UUID.randomUUID().toString());
        kafkaMessage.setBodyCategory(BodyCategory.FHIR);
        kafkaMessage.setBodyType("Bundle");
        kafkaMessage.setContentVersion("R4");

        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        ProducerRecord<String, String> record = new ProducerRecord<String, String>("InputReceived_FHIR_Bundle_MDC8450059",
                kafkaMessage.toString());

        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(20000, TimeUnit.MILLISECONDS);
        assertNotNull(metadata);

        Topic outputTopic = new Topic()
                .setOperation(Operation.DataValidated)
                .setDataCategory(Category.FHIR)
                .setDataType("Bundle")
                .setDataCodes(Arrays.asList("MDC8450059"));
        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);;
        Message outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);

        assertEquals(kafkaMessage.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(kafkaMessage.getTransactionId(), outputMessage.getTransactionId());
        assertTrue(outputMessage.getBody().contains("CTG sent to Milou"));
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        String path = OfflineCTGTest.class.getClassLoader().getResource(".keep_observation_service").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();
        ourLog.info("Project base path is: {}", path);

        Boolean integrationTest = false;
        String integrationTestString = System.getenv("INTEGRATION_TEST");
        if (integrationTestString != null
                && !integrationTestString.isEmpty()
                && integrationTestString.equalsIgnoreCase("true"))
            integrationTest = true;

        //Read and set environment variables from .env file:
        try {
            Properties properties = new Properties();
            FileInputStream environmentFile = new FileInputStream(path + "/" + ENVIRONMENT_FILE);
            ourLog.info("Path: " + path);
            properties.load(environmentFile);
            properties.forEach((k, v)->environmentVariables.set((String)k, (String)v));

            environmentFile = new FileInputStream(path + "/" + DEPLOYMENT_FILE);
            ourLog.info("Path: " + path);
            properties.load(environmentFile);
            properties.forEach((k, v)->environmentVariables.set((String)k, (String)v));
        }
        catch (IOException | IllegalArgumentException e) {
            ourLog.error("Failed loading file from: {}. Error message: {}",  path + "/" + ENVIRONMENT_FILE,
                    e.getMessage());
        }

        fhirObsAsString = ResourceUtil.stringFromResource(observationFileName);
        fhirObsAsStringFail = ResourceUtil.stringFromResource(observationFileNameFail);

        // Instantiate mock Kafka classes
        initMockExternalProducer();
        initMockKafkaConsumeAndProcess();

        FHIR2Milou.main(new String[] {});

        if (!integrationTest) {
            ourLog.debug("Using mocks");

            //Mocks to avoid calling external services during test.
            //Comment out these lines to test getting resource from external service

            Patient mockPatient = new Patient();
            mockPatient.addName().addGiven("Nancy Ann Test").setFamily("Berggren");
            mockPatient.addIdentifier(new Identifier().setSystem("urn:oid:1.2.208.176.1.2").setValue("2512489996"));
            mockPatient.setId("http://example.com/patient_service/1");

            PatientResolver mockResolver = mock(PatientResolver.class);
            FHIR2Milou.patientResolver = mockResolver;
            when(mockResolver.getPatient(any(Reference.class))).thenReturn(mockPatient);

            Device mockDevice = new Device();
            mockDevice.setModelNumber("AN24");
            mockDevice.setSerialNumber("A001962");

            DeviceResolver mockDeviceResolver = mock(DeviceResolver.class);
            FHIR2Milou.deviceResolver = mockDeviceResolver;
            when(mockDeviceResolver.getDevice(any(Reference.class))).thenReturn(mockDevice);

            MilouServerHandler mockMilouSender = mock(MilouServerHandler.class);
            FHIR2Milou.milouResolver = mockMilouSender;
            doNothing().when(mockMilouSender).sendOfflineMessage(null);
        }

    }

    @AfterClass
    public static void afterClass() throws Exception {
        try {
            if (kafkaConsumeAndProcess != null)
                kafkaConsumeAndProcess.stopThread();
            if (mockKafkaConsumeAndProcessThread != null)
                mockKafkaConsumeAndProcessThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void initMockExternalProducer()
            throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        //Read KafkaProducer properties and init producer from these
        InputStream props = Resources.getResource(KAFKA_PRODUCER_PROPS).openStream();
        Properties properties = new Properties();
        properties.load(props);
        Serializer<String> keySerializer = null;
        Serializer<String> valueSerializer = null;

        // Instantiate Kafka Key- and Value-serializers from strings
        keySerializer =
                (Serializer<String>) Class.forName(properties.getProperty("key.serializer")).newInstance();
        valueSerializer =
                (Serializer<String>)Class.forName(properties.getProperty("value.serializer")).newInstance();
        mockExternalKafkaProducer = new MockProducer<>(true, keySerializer, valueSerializer);
    }

    private static void initMockKafkaConsumeAndProcess()
            throws KafkaInitializationException, MessagingInitializationException {
        EventProcessor eventProcessor = new MyEventProcessor();
        mockKafkaEventProducer = new MockKafkaEventProducer(System.getenv("SERVICE_NAME"));

        List<Topic> topics = ServiceUtils.getTopics();
        FHIR2Milou.topics = topics;

        kafkaConsumeAndProcess = new MockKafkaConsumeAndProcess(
                topics,
                mockKafkaEventProducer,
                eventProcessor);
        mockKafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess);
        FHIR2Milou.kafkaConsumeAndProcessThread = mockKafkaConsumeAndProcessThread;
        mockKafkaConsumeAndProcessThread.start();
    }
}