package dk.s4.microservices.milouservice.test;

import dk.s4.microservices.FHIR2MilouService.Exceptions.UnitConverterException;
import dk.s4.microservices.FHIR2MilouService.Utils.DateUtil;
import dk.s4.microservices.FHIR2MilouService.Utils.Utility;
import org.hl7.fhir.r4.model.SampledData;
import org.hl7.fhir.r4.model.SimpleQuantity;
import org.junit.Test;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.*;

public class UtilTest {

    @Test
    public void testDateToCalendar()
    {
        Date date = null;

        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

        try{
            date = isoFormat.parse("2010-05-23T09:01:02+02:00");
        }
        catch (Exception e){
            fail("This should not throw an exception");
        }

        Calendar calendar = DateUtil.date2Calendar(date);

        assertEquals(date.getDate(), calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(date.getDay(), calendar.get(Calendar.DAY_OF_WEEK) - 1);
        assertEquals(date.getMonth(), calendar.get(Calendar.MONTH));
        assertEquals(date.getYear(), calendar.get(Calendar.YEAR) - 1900);
        assertEquals(date.getMinutes(), calendar.get(Calendar.MINUTE));
        assertEquals(date.getHours(), calendar.get(Calendar.HOUR));
        assertEquals(date.getSeconds(), calendar.get(Calendar.SECOND));

        date = null;

        // testing different timezone

        try{
            // date is automatically converted to local timezone
            date = isoFormat.parse("2010-05-23T09:01:02+00:00");
        }
        catch (Exception e){
            fail("This should not throw an exception");
        }


        calendar = DateUtil.date2Calendar(date);

        assertEquals(date.getDate(), calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(date.getDay(), calendar.get(Calendar.DAY_OF_WEEK) - 1);
        assertEquals(date.getMonth(), calendar.get(Calendar.MONTH));
        assertEquals(date.getYear(), calendar.get(Calendar.YEAR) - 1900);
        assertEquals(date.getMinutes(), calendar.get(Calendar.MINUTE));
        assertEquals(date.getHours(), calendar.get(Calendar.HOUR));
        assertEquals(date.getSeconds(), calendar.get(Calendar.SECOND));

    }

    @Test
    public void testCalendarToXMLGregorian(){

        Date date = null;

        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

        try{
            date = isoFormat.parse("2010-05-23T09:01:02+02:00");
        }
        catch (Exception e){
            fail("This should not throw an exception");
        }

        Calendar calendar = DateUtil.date2Calendar(date);
        XMLGregorianCalendar gregCal = null;

        try{
            gregCal = DateUtil.calendar2XMLGregorianCalendar(calendar);
        }
        catch (Exception e){
            fail("This should not throw an exception");
        }

        assertEquals("2010-05-23T07:01:02Z", gregCal.toString());

    }

    @Test
    public void testStringToCalendar(){
        Throwable t = catchThrowable(() -> DateUtil.stringToCalendar("2010-05-23T09:01:02+02:00"));
        assertThat(t).isNull();

        t = catchThrowable(() -> DateUtil.stringToCalendar("2010-05-23T09:01:02+00:00"));
        assertThat(t).isNull();
    }

    @Test
    public void testStringToXMLGregorian(){

        XMLGregorianCalendar xmlGreg = null;

        try{
            xmlGreg = DateUtil.stringToXMLGregorian("2010-05-23T09:01:02+02:00");
        }
        catch (DatatypeConfigurationException e){
            fail("This should not cause an exception");
        }
        catch (ParseException e){
            fail("This should not cause an exception");
        }

        assertEquals("2010-05-23T07:01:02Z", xmlGreg.toString());


    }

    @Test
    public void testMergedSampledData(){

        List<Double> fhr = new ArrayList<>();
        fhr.add(2.2);
        fhr.add(8.2);
        fhr.add(3.2);

        List<Double> mhr = new ArrayList<>();
        mhr.add(9.9);
        mhr.add(5.3);
        mhr.add(2.8);

        List<Double> toco = new ArrayList<>();
        toco.add(4.4);
        toco.add(2.4);
        toco.add(1.4);

        List<Integer> qfhr = new ArrayList<>();
        qfhr.add(2);
        qfhr.add(3);
        qfhr.add(4);

        String result = "";

        result = Utility.mergeSampledData(fhr, mhr, toco, qfhr);

        assertFalse(result.isEmpty());

        assertEquals(result,
                "[2.2, 9.9, 4.4, 2]\n" +
                        "[8.2, 5.3, 2.4, 3]\n" +
                        "[3.2, 2.8, 1.4, 4]\n"
        );

    }

    @Test
    public void testMergedSampledDataWrongListLengths(){

        List<Double> fhr = new ArrayList<>();
        fhr.add(2.2);
        fhr.add(8.2);
        fhr.add(3.2);

        List<Double> mhr = new ArrayList<>();
        mhr.add(9.9);
        mhr.add(2.8);

        List<Double> toco = new ArrayList<>();
        toco.add(4.4);
        toco.add(2.4);
        toco.add(1.4);

        List<Integer> qfhr = new ArrayList<>();
        qfhr.add(2);
        qfhr.add(3);
        qfhr.add(4);

        try {
            String result = Utility.mergeSampledData(fhr, mhr, toco, qfhr);
            fail("Method should have thrown an exception");
        }
        catch (IndexOutOfBoundsException e){

        }
        catch (Exception e){
            fail("Method should not have thrown this exception");
        }

    }

    @Test
    public void testMergedSampledDataEmptyLists(){

        List<Double> fhr = new ArrayList<>();
        List<Double> mhr = new ArrayList<>();
        List<Double> toco = new ArrayList<>();
        List<Integer> qfhr = new ArrayList<>();

        String result = null;

        result = Utility.mergeSampledData(fhr, mhr, toco, qfhr);


        assertNotNull(result);
        assertEquals(result, "");

    }

    @Test
    public void testMergedSampledDataNullLists(){

        List<Double> fhr = new ArrayList<>();
        fhr.add(2.2);
        fhr.add(8.2);
        fhr.add(3.2);

        List<Double> mhr = new ArrayList<>();
        mhr.add(9.9);
        mhr.add(5.3);
        mhr.add(2.8);

        List<Double> toco = null;

        List<Integer> qfhr = new ArrayList<>();
        qfhr.add(2);
        qfhr.add(3);
        qfhr.add(4);

        String result = "";

        try {
            result = Utility.mergeSampledData(fhr, mhr, toco, qfhr);
            fail("Method should have thrown an exception");
        }
        catch (NullPointerException e){

        }
        catch (Exception e){
            fail("Method should not have thrown this exception");
        }

    }

    @Test
    public void testUnitConverter(){

        SampledData sd = new SampledData();
        sd.setData("5 8 10 15");

        SimpleQuantity sq = new SimpleQuantity();
        sq.setValue(5);
        sd.setOrigin(sq);
        sd.setFactor(10);
        int unitFactor = 6;

        ArrayList<Double> expectedResult = new ArrayList<>();
        expectedResult.add(330.0);
        expectedResult.add(510.0);
        expectedResult.add(630.0);
        expectedResult.add(930.0);

        ArrayList<Double> result = null;

        try{
            result = Utility.unitConverter(sd, unitFactor);
        } catch (UnitConverterException e){}

        assertNotNull(result);

        assertEquals(result.size(), expectedResult.size());

        for (int i = 0; i < result.size(); i++){

            if (!result.get(i).equals(expectedResult.get(i))){

                assertEquals(result.get(i), expectedResult.get(i));

            }

        }

    }

    @Test
    public void testUnitConverterNoFactor(){

        SampledData sd = new SampledData();
        sd.setData("5 8");

        SimpleQuantity sq = new SimpleQuantity();
        sq.setValue(5);
        sd.setOrigin(sq);
        int unitFactor = 6;

        ArrayList<Double> expectedResult = new ArrayList<>();
        expectedResult.add(330.0);
        expectedResult.add(510.0);

        try{
            Utility.unitConverter(sd, unitFactor);
            fail("method should have thrown exception");
        } catch (UnitConverterException e){

        }
        catch (Exception e){
            fail("Method snould not have thrown this exception");
        }

    }

    @Test
    public void testUnitConverterNoOrigin(){

        SampledData sd = new SampledData();
        sd.setData("5 8");

        SimpleQuantity sq = new SimpleQuantity();

        sd.setOrigin(sq);
        sd.setFactor(10);
        int unitFactor = 6;

        ArrayList<Double> expectedResult = new ArrayList<>();
        expectedResult.add(330.0);
        expectedResult.add(510.0);

        try{
            Utility.unitConverter(sd, unitFactor);
            fail("method should have thrown exception");
        } catch (UnitConverterException e){

        }
        catch (Exception e){
            fail("Method should not have thrown exception");
        }

    }

    @Test
    public void testUnitConverterNoOrigin2(){

        SampledData sd = new SampledData();
        sd.setData("5 8");

        sd.setFactor(10);
        int unitFactor = 6;

        ArrayList<Double> expectedResult = new ArrayList<>();
        expectedResult.add(330.0);
        expectedResult.add(510.0);

        try{
            Utility.unitConverter(sd, unitFactor);
            fail("method should have thrown exception");
        } catch (UnitConverterException e){

        }
        catch (Exception e){
            fail("Method should not have thrown this exception");
        }


    }

    @Test
    public void testUnitConverterNoData(){

        SampledData sd = new SampledData();
        sd.setData("");

        SimpleQuantity sq = new SimpleQuantity();
        sq.setValue(5);
        sd.setOrigin(sq);
        sd.setFactor(10);
        int unitFactor = 6;

        ArrayList<Double> expectedResult = new ArrayList<>();

        ArrayList<Double> result = null;

        try{
            result = Utility.unitConverter(sd, unitFactor);
        } catch (UnitConverterException e){}

        assertNotNull(result );

        assertEquals(result.size(), expectedResult.size());

        assertEquals(result.size(), 0);

    }

    @Test
    public void testUnitConverterCorruptData(){

        SampledData sd = new SampledData();
        sd.setData("5, 8, 10, 15"); // corrupt by comma

        SimpleQuantity sq = new SimpleQuantity();
        sq.setValue(5);
        sd.setOrigin(sq);
        sd.setFactor(10);
        int unitFactor = 6;

        ArrayList<Double> expectedResult = new ArrayList<>();
        expectedResult.add(330.0);
        expectedResult.add(510.0);
        expectedResult.add(630.0);
        expectedResult.add(930.0);

        ArrayList<Double> result = null;

        try{
            result = Utility.unitConverter(sd, unitFactor);
            fail("Should have thrown an exception");
        } catch (UnitConverterException e){
            assert(true);
        } catch (Exception e){
            fail("Should not have thrown this exception");
        }


    }

    @Test
    public void testUnitConverterNullData(){

        SimpleQuantity sq = new SimpleQuantity();
        sq.setValue(5);
        int unitFactor = 6;

        ArrayList<Double> result = null;

        try{
            result = Utility.unitConverter(null, unitFactor);
            fail("Should have thrown an exception");
        } catch (UnitConverterException e){
            assert(true);
        }
        catch (Exception e){
            fail("Should not have thrown this exception");
        }

    }

    @Test
    public void testCompressString(){

        String input = "Hello world, testing 1, 2, 3...";

        byte[] result = null;
        byte[] expectedResult = { 31, -117, 8, 0, 0, 0, 0, 0, 0, 0, -13, 72, -51, -55, -55, 87, 40, -49, 47, -54, 73, -47, 81, 40, 73, 45, 46, -55, -52, 75, 87, 48, -44, 81, 48, -46, 81, 48, -42, -45, -45, 3, 0, 40, -47, -6, 121, 31, 0, 0, 0 } ;

        try{
            result = Utility.toGZIP(input);
        }
        catch (IOException e){

        }

        assertNotNull(result);
        assertEquals(result.length, expectedResult.length);

        for(int i = 0; i < result.length; i++){
            assertEquals(result[i], expectedResult[i]);
        }


    }

    @Test
    public void testCompressStringEmptyInput(){

        String input = "";

        byte[] result = null;
        byte[] expectedResult = { 31, -117, 8, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0 } ;

        try{
            result = Utility.toGZIP(input);
        }
        catch (IOException e){

        }

        assertNotNull(result);

        assertEquals(result.length, expectedResult.length);

        for(int i = 0; i < result.length; i++){
            assertEquals(result[i], expectedResult[i]);
        }

    }

    @Test
    public void testCompressStringNoInput(){

        String input = null;

        try{
            Utility.toGZIP(input);
            fail("Should throw an exception");
        }
        catch (IOException e){
            fail("Should not throw this exception");
        }
        catch (NullPointerException e){
            assert(true);
        }
        catch(Exception e){
            fail("Should not throw this exception");
        }

    }

}
