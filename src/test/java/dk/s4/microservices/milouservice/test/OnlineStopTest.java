package dk.s4.microservices.milouservice.test;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.google.common.io.Resources;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfdateTime;
import dk.s4.microservices.FHIR2MilouService.Exceptions.FatalFormatException;
import dk.s4.microservices.FHIR2MilouService.Exceptions.FatalInternalErrorException;
import dk.s4.microservices.FHIR2MilouService.Utils.*;
import dk.s4.microservices.FHIR2MilouService.processing.MyEventProcessor;
import dk.s4.microservices.FHIR2MilouService.service.FHIR2Milou;
import dk.s4.microservices.messaging.*;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.messaging.mock.MockKafkaConsumeAndProcess;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.Serializer;
import org.datacontract.schemas._2004._07.milou_server.*;
import org.hl7.fhir.r4.model.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.time.DateTimeException;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class OnlineStopTest {

    private final static Logger ourLog = LoggerFactory.getLogger(OfflineCTGTest.class);

    private static final String observationFile = "FHIR-clean/CTG/online_ex_001/5_consolidated.json";

    private static String observationAsString;

    private static String ENVIRONMENT_FILE = "service.env";
    private static String KAFKA_PRODUCER_PROPS = "producer.props";
    private static String DEPLOYMENT_FILE = "deploy.env";

    private static MockProducer<String, String> mockExternalKafkaProducer;
    private static MockKafkaEventProducer mockKafkaEventProducer;

    private static Thread mockKafkaConsumeAndProcessThread;
    private static  MockKafkaConsumeAndProcess kafkaConsumeAndProcess;

    private static ValidationResult validationResult;

    private static final int FUTURE_TIMEOUT = 5000;

    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testValidationOfObservation(){

        assertEquals( 0, validationResult.getValidationErrors().size());
        assertTrue(validationResult.isFinal());
        assertFalse(validationResult.isPreliminary());
        assertEquals("FINAL", validationResult.getResourceStatus());
        assertEquals(32, validationResult.getSequenceNumber());
        assertEquals("0201919996", validationResult.getPatientRef().getIdentifier().getValue());
        assertEquals("00-80-98-FE-FF-0E-39-13", validationResult.getDeviceReferences().get(0).getIdentifier().getValue());

        try{
            assertEquals(DateUtil.stringToCalendar("2017-06-12T10:48:13+02:00"), validationResult.getStartTime());
            assertEquals(DateUtil.stringToCalendar("2017-06-12T10:48:15+02:00"), validationResult.getEndTime());
        }
        catch (ParseException e){
            fail("This should not throw an exception");
        }

        assertEquals("urn:uuid:d4bcbc32-e9e9-4310-9a30-251f2aaee389", validationResult.getRegistrationId());

        SampledData data = validationResult.getFHR();
        assertEquals("562 561 560 561 562 563 562 561", data.getData());
        assertEquals(250.0, data.getPeriod().doubleValue(), 0);
        assertEquals(0.25, data.getFactor().doubleValue() , 0);
        assertEquals(1, data.getDimensions());
        assertEquals(0, data.getOrigin().getValue().intValue());

        data = validationResult.getMHR();
        assertEquals("322 322 323 323 322 322 323 324", data.getData());
        assertEquals(250.0, data.getPeriod().doubleValue(),  0);
        assertEquals(0.25, data.getFactor().doubleValue(),  0);
        assertEquals(1, data.getDimensions());
        assertEquals(0, data.getOrigin().getValue().intValue());

        data = validationResult.getTOCO();
        assertEquals( "42 41 41 40 42 44 44 43", data.getData());
        assertEquals(250.0, data.getPeriod().doubleValue(),  0);
        assertEquals(1.96, data.getFactor().doubleValue(),  0);
        assertEquals(1, data.getDimensions());
        assertEquals(0, data.getOrigin().getValue().intValue());

        data = validationResult.getSQ();
        assertEquals("2 2 2 1 0 0 1 2", data.getData());
        assertEquals(250.0, data.getPeriod().doubleValue(), 0);
        assertEquals(1, data.getFactor().doubleValue(), 0);
        assertEquals(1, data.getDimensions() );
        assertEquals(0, data.getOrigin().getValue().intValue());

        List<Calendar> markers = validationResult.getMarkers();
        assertEquals(0, markers.size());

    }

    @Test
    public void testSOAPMessageCreatorObservation() throws FatalInternalErrorException, FatalFormatException {

        PatientResolver patientResolver = FHIR2Milou.getPatientResolver();
        Patient patient = patientResolver.getPatient(new Reference());
        CtgMessagePatientName ctgPatientName = new CtgMessagePatientName();
        ctgPatientName.setFirstField(patient.getNameFirstRep().getGivenAsSingleString());
        ctgPatientName.setLastField(patient.getNameFirstRep().getFamily());
        CtgMessagePatient ctgMessagePatient = new CtgMessagePatient();
        ctgMessagePatient.setNameField(ctgPatientName);
        ctgMessagePatient.setIdField("0201919996");

        DeviceResolver deviceResolver = FHIR2Milou.getDeviceResolver();
        Device device = deviceResolver.getDevice(new Reference());

        List<Double> fhr = Arrays.asList(new Double[] { 561.0, 562.0, 562.0, 561.0, 560.0, 561.0, 562.0, 563.0 });
        List<Double> mhr = Arrays.asList(new Double[] { 323.0, 322.0,  322.0,  322.0,  323.0,  323.0,  322.0,  322.0 });
        List<Double> toco = Arrays.asList(new Double[] { 44.0,  41.0,  42.0,  41.0,  41.0,  40.0,  41.0,  43.0});
        List<Integer> sq = Arrays.asList(new Integer[] { 0, 1, 2, 3, 0, 1, 2, 3, });

        ConvertedData convertedData = new ConvertedData();
        convertedData.setTocoConverted(toco);
        convertedData.setMhrConverted(mhr);
        convertedData.setSqConverted(sq);
        convertedData.setFhrConverted(fhr);

        CtgMessage message = null;

        try{
            message = SOAPMessageCreator.createOnlineMessage(ctgMessagePatient, validationResult, convertedData, device);
        }
        catch (DateTimeException e){
            fail("Should not cause exception");
        }
        catch (DatatypeConfigurationException e){
            fail("Should not cause exception");
        }

        assertEquals("AN24 A001286", message.getDeviceIDField());
        assertEquals(0, message.getTocoShiftField());

        ArrayOfdateTime markers = message.getMarkersField();
        assertEquals(0, markers.getDateTime().size());

        CtgMessagePatient ctgPatient = message.getPatientField();
        CtgMessagePatientName name = ctgPatient.getNameField();
        assertEquals("Nancy Ann Test", name.getFirstField() );
        assertEquals("Berggren", name.getLastField());
        assertEquals("0201919996", ctgPatient.getIdField());

        assertEquals("urn:uuid:d4bcbc32-e9e9-4310-9a30-251f2aaee389", message.getRegistrationIDField());

        ArrayOfCtgMessageBlock blocksArray = message.getCtgField();
        List<CtgMessageBlock> blocks = blocksArray.getCtgMessageBlock();

        assertEquals(2, blocks.size());

        CtgMessageBlock first = blocks.get(0);

        assertEquals("[561.0, 562.0, 562.0, 561.0]", first.getFhrField());
        assertEquals("[323.0, 322.0, 322.0, 322.0]", first.getMhrField());
        assertEquals("[0, 1, 2, 3]", first.getSqField());
        assertEquals("[44.0, 41.0, 42.0, 41.0]", first.getTocoField());
        assertEquals(33, first.getSequenceNbrField());

        CtgMessageBlock second = blocks.get(1);
        assertEquals("[560.0, 561.0, 562.0, 563.0]", second.getFhrField());
        assertEquals("[323.0, 323.0, 322.0, 322.0]", second.getMhrField());
        assertEquals("[0, 1, 2, 3]", second.getSqField());
        assertEquals("[41.0, 40.0, 41.0, 43.0]", second.getTocoField());
        assertEquals(34, second.getSequenceNbrField());

        assertEquals("2017-06-12T08:48:13Z", first.getTimeField().toString());
        assertEquals("2017-06-12T08:48:14Z", second.getTimeField().toString());

    }

    @Test
    public void testSendOnlineObservation2Kafka() throws Exception {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);

        Message kafkaMessage = new Message();
        kafkaMessage.setBody(observationAsString);
        kafkaMessage.setSender("TestProducer");
        kafkaMessage.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        kafkaMessage.setTransactionId(UUID.randomUUID().toString());
        kafkaMessage.setBodyType("FHIR");
        kafkaMessage.setContentVersion("R4");

        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        ProducerRecord<String, String> record = new ProducerRecord<String, String>("InputReceived_FHIR_Observation_MDC8450059",
                kafkaMessage.toString());

        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(15000, TimeUnit.MILLISECONDS);
        assertNotNull(metadata);

        Topic outputTopic = new Topic()
                .setOperation(Operation.DataValidated)
                .setDataCategory(Category.FHIR)
                .setDataType("Observation")
                .setDataCodes(Arrays.asList("MDC8450059"));
        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);;
        Message outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);

        assertEquals(kafkaMessage.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(kafkaMessage.getTransactionId(), outputMessage.getTransactionId());
        assertTrue(outputMessage.getBody().contains("Success"));
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        String path = OfflineCTGTest.class.getClassLoader().getResource(".keep_observation_service").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();
        ourLog.info("Project base path is: {}", path);

        boolean integrationTest = false;
        String integrationTestString = System.getenv("INTEGRATION_TEST");
        if (integrationTestString != null
                && !integrationTestString.isEmpty()
                && integrationTestString.equalsIgnoreCase("true"))
            integrationTest = true;

        //Read and set environment variables from .env file:
        try {
            Properties properties = new Properties();
            FileInputStream environmentFile = new FileInputStream(path + "/" + ENVIRONMENT_FILE);
            ourLog.info("Path: " + path);
            properties.load(environmentFile);
            properties.forEach((k, v)->environmentVariables.set((String)k, (String)v));

            environmentFile = new FileInputStream(path + "/" + DEPLOYMENT_FILE);
            ourLog.info("Path: " + path);
            properties.load(environmentFile);
            properties.forEach((k, v)->environmentVariables.set((String)k, (String)v));
        }
        catch (IOException | IllegalArgumentException e) {
            ourLog.error("Failed loading file from: {}. Error message: {}",  path + "/" + ENVIRONMENT_FILE,
                    e.getMessage());
        }

        // Instantiate mock Kafka classes
        initMockExternalProducer();
        initMockKafkaConsumeAndProcess();

        observationAsString = ResourceUtil.stringFromResource(observationFile);

        FHIR2Milou.main(new String[] {});

        if (!integrationTest) {
            ourLog.debug("Using mocks");

            //Mocks to avoid calling external services during test.
            //Comment out these lines to test getting resource from external service

            Device mockDevice = new Device();
            mockDevice.setModelNumber("AN24");
            mockDevice.setSerialNumber("A001286");

            DeviceResolver mockDeviceResolver = mock(DeviceResolver.class);
            FHIR2Milou.deviceResolver = mockDeviceResolver;
            when(mockDeviceResolver.getDevice(any(Reference.class))).thenReturn(mockDevice);

            Patient mockPatient = new Patient();
            mockPatient.addName().addGiven("Nancy Ann Test").setFamily("Berggren");
            mockPatient.addIdentifier(new Identifier().setSystem("urn:oid:1.2.208.176.1.2").setValue("2512489996"));
            mockPatient.setId("http://example.com/patient_service/1");

            PatientResolver mockResolver = mock(PatientResolver.class);
            FHIR2Milou.patientResolver = mockResolver;
            when(mockResolver.getPatient(any(Reference.class))).thenReturn(mockPatient);

            MilouServerHandler mockMilouSender = mock(MilouServerHandler.class);
            FHIR2Milou.milouResolver = mockMilouSender;
            doNothing().when(mockMilouSender).sendOfflineMessage(null);
            doNothing().when(mockMilouSender).sendOnlineMessage(any((CtgMessage.class)));
            doNothing().when(mockMilouSender).sendOnlineStopMessage(any(String.class) ,any(String.class), any(XMLGregorianCalendar.class));


        }

        IParser parser = FhirContext.forR4().newJsonParser();
        CTGValidator validator = new CTGValidator();

        Observation observation = parser.parseResource(Observation.class, observationAsString);

        validationResult = validator.validate(observation);

    }

    @AfterClass
    public static void afterClass() throws Exception {
        try {
            if (kafkaConsumeAndProcess != null)
                kafkaConsumeAndProcess.stopThread();
            if (mockKafkaConsumeAndProcessThread != null)
                mockKafkaConsumeAndProcessThread.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void initMockExternalProducer()
            throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        //Read KafkaProducer properties and init producer from these
        InputStream props = Resources.getResource(KAFKA_PRODUCER_PROPS).openStream();
        Properties properties = new Properties();
        properties.load(props);
        Serializer<String> keySerializer = null;
        Serializer<String> valueSerializer = null;

        // Instantiate Kafka Key- and Value-serializers from strings
        keySerializer =
                (Serializer<String>) Class.forName(properties.getProperty("key.serializer")).newInstance();
        valueSerializer =
                (Serializer<String>)Class.forName(properties.getProperty("value.serializer")).newInstance();
        mockExternalKafkaProducer = new MockProducer<>(true, keySerializer, valueSerializer);
    }

    private static void initMockKafkaConsumeAndProcess()
            throws KafkaInitializationException, MessagingInitializationException {
        EventProcessor eventProcessor = new MyEventProcessor();
        mockKafkaEventProducer = new MockKafkaEventProducer(System.getenv("SERVICE_NAME"));

        List<Topic> topics = ServiceUtils.getTopics();
        FHIR2Milou.topics = topics;

        kafkaConsumeAndProcess = new MockKafkaConsumeAndProcess(
                topics,
                mockKafkaEventProducer,
                eventProcessor);
        mockKafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess);


        FHIR2Milou.kafkaConsumeAndProcessThread = mockKafkaConsumeAndProcessThread;

        mockKafkaConsumeAndProcessThread.start();
    }

}
