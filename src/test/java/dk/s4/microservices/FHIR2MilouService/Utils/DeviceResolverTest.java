package dk.s4.microservices.FHIR2MilouService.Utils;

import static com.github.tomakehurst.wiremock.client.WireMock.anyUrl;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import dk.s4.microservices.FHIR2MilouService.Exceptions.FatalInternalErrorException;
import dk.s4.microservices.microservicecommon.TestUtils;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.Mockito;

public class DeviceResolverTest {

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());

    private String serviceUrl;

    /**
     * Test that deviceResolver.getDevice keeps retrying as long as we get an empty Bundle result from the service
     * storing Device resources. Also check that we give up and throw an exception when we have tried DEVICE_RESOLVER_RETRIES
     * number of times.
     * @throws Exception
     */
    @Test
    public void getDeviceNoMatchingDevice() throws Exception {
        environmentVariables.set("DEVICE_RESOLVER_RETRIES", "5");

        DeviceResolver deviceResolver = Mockito.spy(new DeviceResolver(serviceUrl));
        Reference deviceReference = new Reference()
                .setType("Device")
                .setIdentifier(new Identifier().setSystem("urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680").setValue("00-80-98-FE-FF-0E-39-13"));

        serviceUrl = "http://localhost:" + wireMockRule.port();
        stubFor(get(urlPathMatching("/Device"))
                        .willReturn(okJson(EMPTY_BUNDLE)
                                            .withStatus(200)
                                            .withHeader("Content-Type", "application/json")
                                   )
               );

        Throwable thrown = catchThrowable(() -> deviceResolver.getDevice(deviceReference));;

        WireMock.verify(5, getRequestedFor(anyUrl()));
        assertTrue(thrown instanceof FatalInternalErrorException);
        assertTrue(thrown.getMessage().contains("No matching Device could not be looked up at"));
    }

    @Test
    public void getDeviceMatchingDevice() throws Exception {
        DeviceResolver deviceResolver = Mockito.spy(new DeviceResolver(serviceUrl));
        Reference deviceReference = new Reference()
                .setType("Device")
                .setIdentifier(new Identifier().setSystem("urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680").setValue("00-80-98-FE-FF-0E-39-13"));

        serviceUrl = "http://localhost:" + wireMockRule.port();
        stubFor(get(urlPathMatching("/Device"))
                        .willReturn(okJson(MATCHED_BUNDLE)
                                            .withStatus(200)
                                            .withHeader("Content-Type", "application/json")
                                   )
               );

        Device device = deviceResolver.getDevice(deviceReference);

        WireMock.verify(1, getRequestedFor(anyUrl()));
        assertEquals(device.getIdentifier().get(0).getSystem(), "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680");
        assertEquals(device.getIdentifier().get(0).getValue(), "00-80-98-FE-FF-0E-39-13");
    }

    @Before
    public void before() throws Exception {
        TestUtils.readEnvironment("service.env", environmentVariables);
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        serviceUrl = "http://localhost:" + wireMockRule.port();
    }

    private static final String EMPTY_BUNDLE = "{\n"
            + "  \"resourceType\": \"Bundle\",\n"
            + "  \"id\": \"460aab92-37cd-42ed-ae63-73eaccf4fea4\",\n"
            + "  \"meta\": {\n"
            + "    \"lastUpdated\": \"2019-10-23T06:28:43.816+00:00\"\n"
            + "  },\n"
            + "  \"type\": \"searchset\",\n"
            + "  \"total\": 0,\n"
            + "  \"link\": [\n"
            + "    {\n"
            + "      \"relation\": \"self\",\n"
            + "      \"url\": \"http://hapi.fhir.org/baseR4/Device?_pretty=true&identifier=adf%7Cadf\"\n"
            + "    }\n"
            + "  ]\n"
            + "}";
    private static final String MATCHED_BUNDLE = "{\n"
            + "  \"resourceType\": \"Bundle\",\n"
            + "  \"id\": \"33dcb6bd-8cef-4bd5-8282-091132146337\",\n"
            + "  \"meta\": {\n"
            + "    \"lastUpdated\": \"2019-10-23T08:02:52.039+00:00\"\n"
            + "  },\n"
            + "  \"type\": \"searchset\",\n"
            + "  \"link\": [\n"
            + "    {\n"
            + "      \"relation\": \"self\",\n"
            + "      \"url\": \"http://example.com/baseR4/Device?_format=json&_pretty=true\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"relation\": \"next\",\n"
            + "      \"url\": \"http://example.com/baseR4?_getpages=33dcb6bd-8cef-4bd5-8282-091132146337&_getpagesoffset=20&_count=20&_format=json&_pretty=true&_bundletype=searchset\"\n"
            + "    }\n"
            + "  ],\n"
            + "  \"entry\": [\n"
            + "    {\n"
            + "      \"fullUrl\": \"http://example.com/baseR4/Device/7\",\n"
            + "      \"resource\": \n"
            + "      {\n"
            + "        \"resourceType\": \"Device\",\n"
            + "        \"id\": \"008098FEFF0E3913.0080980E3913\",\n"
            + "        \"meta\": {\n"
            + "          \"profile\": [\n"
            + "            \"http://hl7.org/fhir/uv/phd/StructureDefinition/PhdDevice\"\n"
            + "          ]\n"
            + "        },\n"
            + "        \"identifier\": [\n"
            + "          {\n"
            + "            \"type\": {\n"
            + "              \"coding\": [{\n"
            + "                \"system\": \"http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaDeviceIdentifiers\",\n"
            + "                \"code\": \"SYSID\"\n"
            + "              }]\n"
            + "            },\n"
            + "            \"system\": \"urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680\",\n"
            + "            \"value\": \"00-80-98-FE-FF-0E-39-13\"\n"
            + "          }\n"
            + "        ],\n"
            + "        \"type\": {\n"
            + "          \"coding\": [\n"
            + "            {\n"
            + "              \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "              \"code\": \"65573\",\n"
            + "              \"display\": \"MDC_MOC_VMS_MDS_SIMP\"\n"
            + "            }\n"
            + "          ],\n"
            + "          \"text\": \"MDC_MOC_VMS_MDS_SIMP: Continua Personal Health Device\"\n"
            + "        },\n"
            + "        \"specialization\": [\n"
            + "          {\n"
            + "            \"systemType\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "                  \"code\": \"585728\",\n"
            + "                  \"display\": \"MDC_DEV_PLACEHOLDER_SPEC_PROFILE_CTG\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"MDC_DEV_PLACEHOLDER_SPEC_PROFILE_CTG: CTG recorder\"\n"
            + "            },\n"
            + "            \"version\": \"0\"\n"
            + "          }\n"
            + "        ],\n"
            + "        \"manufacturer\": \"Monica Healthcare\",\n"
            + "        \"modelNumber\": \"AN24V1\",\n"
            + "        \"serialNumber\": \"A001286\",\n"
            + "        \"version\": [\n"
            + "          {\n"
            + "            \"type\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "                  \"code\": \"531975\",\n"
            + "                  \"display\": \"MDC_ID_PROD_SPEC_SW\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"Software revision\"\n"
            + "            },\n"
            + "            \"value\": \"A.02.00\"\n"
            + "          },{\n"
            + "            \"type\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "                  \"code\": \"531976\",\n"
            + "                  \"display\": \"MDC_ID_PROD_SPEC_FW\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"Firmware revision\"\n"
            + "            },\n"
            + "            \"value\": \"6.520\"\n"
            + "          },{\n"
            + "            \"type\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "                  \"code\": \"531977\",\n"
            + "                  \"display\": \"MDC_ID_PROD_SPEC_PROTOCOL\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"Protocol revision\"\n"
            + "            },\n"
            + "            \"value\": \"A.03.00\"\n"
            + "          },{\n"
            + "            \"type\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "                  \"code\": \"532352\",\n"
            + "                  \"display\": \"MDC_REG_CERT_DATA_CONTINUA_VERSION\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"Continua Design Guidelines version\"\n"
            + "            },\n"
            + "            \"value\": \"07.00\"\n"
            + "          }\n"
            + "        ],\n"
            + "        \"property\": [\n"
            + "          {\n"
            + "            \"type\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"http://hl7.org/fhir/uv/phd/CodeSystem/ASN1ToHL7\",\n"
            + "                  \"code\": \"532354.0\",\n"
            + "                  \"display\": \"regulation-status\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"Regulation status\"\n"
            + "            },\n"
            + "            \"valueCode\": [\n"
            + "              {\n"
            + "                \"coding\": [\n"
            + "                  {\n"
            + "                    \"system\": \"http://terminology.hl7.org/CodeSystem/v2-0136\",\n"
            + "                    \"code\": \"N\",\n"
            + "                    \"display\": \"regulated\"\n"
            + "                  }\n"
            + "                ],\n"
            + "                \"text\": \"Regulated medical device\"\n"
            + "              }\n"
            + "            ]\n"
            + "          }\n"
            + "        ]\n"
            + "      },\n"
            + "      \"search\": {\n"
            + "        \"mode\": \"match\"\n"
            + "      }\n"
            + "    }\n"
            + "  ]\n"
            + "}\n";
}