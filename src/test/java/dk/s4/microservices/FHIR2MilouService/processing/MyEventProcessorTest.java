package dk.s4.microservices.FHIR2MilouService.processing;

import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import static org.mockito.Mockito.when;

public class MyEventProcessorTest {
    MyEventProcessor processor;

    @Before
    public void setup() {
        processor = new MyEventProcessor();
    }

    @Test
    public void singleValidReferenceDoesNotThrow() {
        processor.validate(Arrays.asList(
            aReferenceWith("system", "value")
        ));
    }

    @Test
    public void severalValidReferencesDoesNotThrow() {
        processor.validate(Arrays.asList(
                aReferenceWith("system", "value"),
                aReferenceWith("system", "value"),
                aReferenceWith("system", "value"),
                aReferenceWith("system", "value"),
                aReferenceWith("system", "value")
        ));
    }

    @Test
    public void severalDifferentValuesDoesNotThrow() {
        processor.validate(Arrays.asList(
                aReferenceWith("system", "value1"),
                aReferenceWith("system", "value2"),
                aReferenceWith("system", "value3"),
                aReferenceWith("system", "value4"),
                aReferenceWith("system", "value5")
        ));
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyReferenceFails() {
        processor.validate(Arrays.asList(
                Mockito.mock(Reference.class)
        ));
    }

    @Test(expected = IllegalArgumentException.class)
    public void referenceWithoutIdentifierFails() {
        Reference mock = Mockito.mock(Reference.class);
        when(mock.hasIdentifier()).thenReturn(false);
        processor.validate(Arrays.asList(
                mock
        ));
    }

    @Test(expected = IllegalArgumentException.class)
    public void aSingleDifferenceValueFails() {
        processor.validate(Arrays.asList(
                aReferenceWith("system", "value1"),
                aReferenceWith("system", "value2"),
                aReferenceWith("system", "value3"),
                aReferenceWith("system", "value4"),
                aReferenceWith("system1", "value5")
        ));
    }

    @Test(expected = IllegalArgumentException.class)
    public void valueIsRequired() {
        processor.validate(Arrays.asList(
                aReferenceWith("system", "value1"),
                aReferenceWith("system", "value2"),
                aReferenceWith("system", "value3"),
                aReferenceWith("system", "value4"),
                aReferenceWith("system", null)
        ));
    }

    @Test(expected = IllegalArgumentException.class)
    public void systemIsRequired() {
        processor.validate(Arrays.asList(
                aReferenceWith("system", "value1"),
                aReferenceWith("system", "value2"),
                aReferenceWith("system", "value3"),
                aReferenceWith("system", "value4"),
                aReferenceWith( null,"value5")
        ));
    }

    private Reference aReferenceWith(String system, String value) {
        Identifier identifier = new Identifier();
        identifier.setSystem(system);
        identifier.setValue(value);
        Reference mock = Mockito.mock(Reference.class);
        when(mock.getIdentifier()).thenReturn(identifier);
        when(mock.hasIdentifier()).thenReturn(true);
        return mock;
    }
}