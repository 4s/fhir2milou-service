package dk.s4.microservices.FHIR2MilouService.processing;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class CtgTypeTest {

    @Test
    public void testOnlineBundle(){
        CtgType ctgType = CtgType.from(new Bundle(), false);
        assertThat(ctgType.isOnline()).isTrue();
        assertThat(ctgType).isInstanceOf(CtgType.ONLINE_BUNDLE.getClass());
    }

    @Test
    public void testOfflineBundleFinal(){
        CtgType ctgType = CtgType.from(new Bundle(), true);
        assertThat(ctgType.isOnline()).isFalse();
        assertThat(ctgType).isInstanceOf(CtgType.OFFLINE_BUNDLE.getClass());
    }

    @Test
    public void testOnlineObservation(){
        CtgType ctgType = CtgType.from(new Observation(), false);
        assertThat(ctgType.isOnline()).isTrue();
        assertThat(ctgType).isInstanceOf(CtgType.ONLINE_OBSERVATION.getClass());
    }

    @Test
    public void testOnlineObservationFinal(){
        CtgType ctgType = CtgType.from(new Observation(), true);
        assertThat(ctgType.isOnline()).isTrue();
        assertThat(ctgType).isInstanceOf(CtgType.ONLINE_OBSERVATION.getClass());
    }

    @Test
    public void testNullPointerException(){
        Throwable throwable = catchThrowable(() -> CtgType.from(null, false));
        assertThat(throwable).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void testNullPointerExceptionFinal(){
        Throwable throwable = catchThrowable(() -> CtgType.from(null, true));
        assertThat(throwable).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void testRuntimeException() {
        Throwable throwable = catchThrowable(() -> CtgType.from(new Patient(), false));
        assertThat(throwable).isInstanceOf(RuntimeException.class);
    }

    @Test
    public void testRuntimeExceptionFinal() {
        Throwable throwable = catchThrowable(() -> CtgType.from(new Patient(), true));
        assertThat(throwable).isInstanceOf(RuntimeException.class);
    }

}