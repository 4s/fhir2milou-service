
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.datacontract.schemas._2004._07.milou_server.CtgMessage;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _NewMessageMessage_QNAME = new QName("http://tempuri.org/", "message");
    private final static QName _StopRegistrationDeviceID_QNAME = new QName("http://tempuri.org/", "deviceID");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NewMessage }
     * 
     */
    public NewMessage createNewMessage() {
        return new NewMessage();
    }

    /**
     * Create an instance of {@link NewMessageResponse }
     * 
     */
    public NewMessageResponse createNewMessageResponse() {
        return new NewMessageResponse();
    }

    /**
     * Create an instance of {@link StopRegistration }
     * 
     */
    public StopRegistration createStopRegistration() {
        return new StopRegistration();
    }

    /**
     * Create an instance of {@link StopRegistrationResponse }
     * 
     */
    public StopRegistrationResponse createStopRegistrationResponse() {
        return new StopRegistrationResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CtgMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "message", scope = NewMessage.class)
    public JAXBElement<CtgMessage> createNewMessageMessage(CtgMessage value) {
        return new JAXBElement<CtgMessage>(_NewMessageMessage_QNAME, CtgMessage.class, NewMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "deviceID", scope = StopRegistration.class)
    public JAXBElement<String> createStopRegistrationDeviceID(String value) {
        return new JAXBElement<String>(_StopRegistrationDeviceID_QNAME, String.class, StopRegistration.class, value);
    }

}
