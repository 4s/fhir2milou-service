
package org.datacontract.schemas._2004._07.milou_server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CtgMessageBlock complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CtgMessageBlock"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fhrField" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mhrField" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="sequenceNbrField" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="sqField" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="timeField" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="tocoField" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtgMessageBlock", propOrder = {
    "fhrField",
    "mhrField",
    "sequenceNbrField",
    "sqField",
    "timeField",
    "tocoField"
})
public class CtgMessageBlock {

    @XmlElement(required = true, nillable = true)
    protected String fhrField;
    @XmlElement(required = true, nillable = true)
    protected String mhrField;
    protected int sequenceNbrField;
    @XmlElement(required = true, nillable = true)
    protected String sqField;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeField;
    @XmlElement(required = true, nillable = true)
    protected String tocoField;

    /**
     * Gets the value of the fhrField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFhrField() {
        return fhrField;
    }

    /**
     * Sets the value of the fhrField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFhrField(String value) {
        this.fhrField = value;
    }

    /**
     * Gets the value of the mhrField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMhrField() {
        return mhrField;
    }

    /**
     * Sets the value of the mhrField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMhrField(String value) {
        this.mhrField = value;
    }

    /**
     * Gets the value of the sequenceNbrField property.
     * 
     */
    public int getSequenceNbrField() {
        return sequenceNbrField;
    }

    /**
     * Sets the value of the sequenceNbrField property.
     * 
     */
    public void setSequenceNbrField(int value) {
        this.sequenceNbrField = value;
    }

    /**
     * Gets the value of the sqField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSqField() {
        return sqField;
    }

    /**
     * Sets the value of the sqField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSqField(String value) {
        this.sqField = value;
    }

    /**
     * Gets the value of the timeField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeField() {
        return timeField;
    }

    /**
     * Sets the value of the timeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeField(XMLGregorianCalendar value) {
        this.timeField = value;
    }

    /**
     * Gets the value of the tocoField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTocoField() {
        return tocoField;
    }

    /**
     * Sets the value of the tocoField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTocoField(String value) {
        this.tocoField = value;
    }

}
