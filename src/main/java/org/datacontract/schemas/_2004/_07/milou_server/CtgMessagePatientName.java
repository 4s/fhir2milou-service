
package org.datacontract.schemas._2004._07.milou_server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CtgMessagePatientName complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CtgMessagePatientName"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="firstField" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="lastField" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtgMessagePatientName", propOrder = {
    "firstField",
    "lastField"
})
public class CtgMessagePatientName {

    @XmlElement(required = true, nillable = true)
    protected String firstField;
    @XmlElement(required = true, nillable = true)
    protected String lastField;

    /**
     * Gets the value of the firstField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstField() {
        return firstField;
    }

    /**
     * Sets the value of the firstField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstField(String value) {
        this.firstField = value;
    }

    /**
     * Gets the value of the lastField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastField() {
        return lastField;
    }

    /**
     * Sets the value of the lastField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastField(String value) {
        this.lastField = value;
    }

}
