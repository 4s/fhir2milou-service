
package org.datacontract.schemas._2004._07.milou_server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CtgMessagePatient complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CtgMessagePatient"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idField" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nameField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT}CtgMessagePatientName"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtgMessagePatient", propOrder = {
    "idField",
    "nameField"
})
public class CtgMessagePatient {

    @XmlElement(required = true, nillable = true)
    protected String idField;
    @XmlElement(required = true, nillable = true)
    protected CtgMessagePatientName nameField;

    /**
     * Gets the value of the idField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdField() {
        return idField;
    }

    /**
     * Sets the value of the idField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdField(String value) {
        this.idField = value;
    }

    /**
     * Gets the value of the nameField property.
     * 
     * @return
     *     possible object is
     *     {@link CtgMessagePatientName }
     *     
     */
    public CtgMessagePatientName getNameField() {
        return nameField;
    }

    /**
     * Sets the value of the nameField property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtgMessagePatientName }
     *     
     */
    public void setNameField(CtgMessagePatientName value) {
        this.nameField = value;
    }

}
