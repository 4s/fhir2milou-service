
package org.datacontract.schemas._2004._07.milou_server;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.milou_server package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CtgMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT", "CtgMessage");
    private final static QName _ArrayOfCtgMessageBlock_QNAME = new QName("http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT", "ArrayOfCtgMessageBlock");
    private final static QName _CtgMessageBlock_QNAME = new QName("http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT", "CtgMessageBlock");
    private final static QName _CtgMessagePatient_QNAME = new QName("http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT", "CtgMessagePatient");
    private final static QName _CtgMessagePatientName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT", "CtgMessagePatientName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.milou_server
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CtgMessage }
     * 
     */
    public CtgMessage createCtgMessage() {
        return new CtgMessage();
    }

    /**
     * Create an instance of {@link ArrayOfCtgMessageBlock }
     * 
     */
    public ArrayOfCtgMessageBlock createArrayOfCtgMessageBlock() {
        return new ArrayOfCtgMessageBlock();
    }

    /**
     * Create an instance of {@link CtgMessageBlock }
     * 
     */
    public CtgMessageBlock createCtgMessageBlock() {
        return new CtgMessageBlock();
    }

    /**
     * Create an instance of {@link CtgMessagePatient }
     * 
     */
    public CtgMessagePatient createCtgMessagePatient() {
        return new CtgMessagePatient();
    }

    /**
     * Create an instance of {@link CtgMessagePatientName }
     * 
     */
    public CtgMessagePatientName createCtgMessagePatientName() {
        return new CtgMessagePatientName();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CtgMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT", name = "CtgMessage")
    public JAXBElement<CtgMessage> createCtgMessage(CtgMessage value) {
        return new JAXBElement<CtgMessage>(_CtgMessage_QNAME, CtgMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCtgMessageBlock }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT", name = "ArrayOfCtgMessageBlock")
    public JAXBElement<ArrayOfCtgMessageBlock> createArrayOfCtgMessageBlock(ArrayOfCtgMessageBlock value) {
        return new JAXBElement<ArrayOfCtgMessageBlock>(_ArrayOfCtgMessageBlock_QNAME, ArrayOfCtgMessageBlock.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CtgMessageBlock }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT", name = "CtgMessageBlock")
    public JAXBElement<CtgMessageBlock> createCtgMessageBlock(CtgMessageBlock value) {
        return new JAXBElement<CtgMessageBlock>(_CtgMessageBlock_QNAME, CtgMessageBlock.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CtgMessagePatient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT", name = "CtgMessagePatient")
    public JAXBElement<CtgMessagePatient> createCtgMessagePatient(CtgMessagePatient value) {
        return new JAXBElement<CtgMessagePatient>(_CtgMessagePatient_QNAME, CtgMessagePatient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CtgMessagePatientName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT", name = "CtgMessagePatientName")
    public JAXBElement<CtgMessagePatientName> createCtgMessagePatientName(CtgMessagePatientName value) {
        return new JAXBElement<CtgMessagePatientName>(_CtgMessagePatientName_QNAME, CtgMessagePatientName.class, null, value);
    }

}
