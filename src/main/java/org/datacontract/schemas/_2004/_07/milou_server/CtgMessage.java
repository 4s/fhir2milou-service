
package org.datacontract.schemas._2004._07.milou_server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfdateTime;


/**
 * <p>Java class for CtgMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CtgMessage"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ctgField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT}ArrayOfCtgMessageBlock"/&gt;
 *         &lt;element name="deviceIDField" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="markersField" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfdateTime"/&gt;
 *         &lt;element name="patientField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT}CtgMessagePatient"/&gt;
 *         &lt;element name="registrationIDField" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tocoShiftField" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtgMessage", propOrder = {
    "ctgField",
    "deviceIDField",
    "markersField",
    "patientField",
    "registrationIDField",
    "tocoShiftField"
})
public class CtgMessage {

    @XmlElement(required = true, nillable = true)
    protected ArrayOfCtgMessageBlock ctgField;
    @XmlElement(required = true, nillable = true)
    protected String deviceIDField;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfdateTime markersField;
    @XmlElement(required = true, nillable = true)
    protected CtgMessagePatient patientField;
    @XmlElement(required = true, nillable = true)
    protected String registrationIDField;
    protected int tocoShiftField;

    /**
     * Gets the value of the ctgField property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCtgMessageBlock }
     *     
     */
    public ArrayOfCtgMessageBlock getCtgField() {
        return ctgField;
    }

    /**
     * Sets the value of the ctgField property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCtgMessageBlock }
     *     
     */
    public void setCtgField(ArrayOfCtgMessageBlock value) {
        this.ctgField = value;
    }

    /**
     * Gets the value of the deviceIDField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceIDField() {
        return deviceIDField;
    }

    /**
     * Sets the value of the deviceIDField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceIDField(String value) {
        this.deviceIDField = value;
    }

    /**
     * Gets the value of the markersField property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfdateTime }
     *     
     */
    public ArrayOfdateTime getMarkersField() {
        return markersField;
    }

    /**
     * Sets the value of the markersField property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfdateTime }
     *     
     */
    public void setMarkersField(ArrayOfdateTime value) {
        this.markersField = value;
    }

    /**
     * Gets the value of the patientField property.
     * 
     * @return
     *     possible object is
     *     {@link CtgMessagePatient }
     *     
     */
    public CtgMessagePatient getPatientField() {
        return patientField;
    }

    /**
     * Sets the value of the patientField property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtgMessagePatient }
     *     
     */
    public void setPatientField(CtgMessagePatient value) {
        this.patientField = value;
    }

    /**
     * Gets the value of the registrationIDField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationIDField() {
        return registrationIDField;
    }

    /**
     * Sets the value of the registrationIDField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationIDField(String value) {
        this.registrationIDField = value;
    }

    /**
     * Gets the value of the tocoShiftField property.
     * 
     */
    public int getTocoShiftField() {
        return tocoShiftField;
    }

    /**
     * Sets the value of the tocoShiftField property.
     * 
     */
    public void setTocoShiftField(int value) {
        this.tocoShiftField = value;
    }

}
