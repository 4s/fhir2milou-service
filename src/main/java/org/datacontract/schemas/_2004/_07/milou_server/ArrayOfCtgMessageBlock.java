
package org.datacontract.schemas._2004._07.milou_server;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCtgMessageBlock complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCtgMessageBlock"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CtgMessageBlock" type="{http://schemas.datacontract.org/2004/07/Milou.Server.OpenTeleRT}CtgMessageBlock" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCtgMessageBlock", propOrder = {
    "ctgMessageBlock"
})
public class ArrayOfCtgMessageBlock {

    @XmlElement(name = "CtgMessageBlock", nillable = true)
    protected List<CtgMessageBlock> ctgMessageBlock;

    /**
     * Gets the value of the ctgMessageBlock property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ctgMessageBlock property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCtgMessageBlock().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CtgMessageBlock }
     * 
     * 
     */
    public List<CtgMessageBlock> getCtgMessageBlock() {
        if (ctgMessageBlock == null) {
            ctgMessageBlock = new ArrayList<CtgMessageBlock>();
        }
        return this.ctgMessageBlock;
    }

}
