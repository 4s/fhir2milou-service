package dk.s4.microservices.FHIR2MilouService.Utils;

import dk.s4.microservices.messaging.Topic;

import java.util.ArrayList;
import java.util.List;

public class ServiceUtils {

    public static List<Topic> getTopics(){

        ArrayList<String> dataCodes = new ArrayList<>();
        dataCodes.add("MDC8450059");

        ArrayList<Topic> topics = new ArrayList<>();
        Topic topic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .setDataCodes(dataCodes);
        topics.add(topic);

        Topic topic_observation = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Observation")
                .setDataCodes(dataCodes);
        topics.add(topic_observation);

        return topics;

    }
}
