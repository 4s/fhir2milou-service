package dk.s4.microservices.FHIR2MilouService.Utils;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;
import milou.opentelert.RTService;
import milouWsdlCode.CtgImportService;
import milouWsdlCode.ICtgImport;
import milouWsdlCode.Registration;
import org.datacontract.schemas._2004._07.milou_server.CtgMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;

/**
 * Class for sending SOAP messages to the Milou Server
 */

public class MilouServerHandler {

    private final Logger logger = LoggerFactory.getLogger(MilouServerHandler.class);

    /**
     * Method for sending an offline CTG message to the MILOU-server
     * @param message The message to send
     * @throws Exception
     */

    public void sendOfflineMessage(Registration message) {

        CtgImportService service;

        try {
            logger.info("Instantiating MILOU web service reference.");
            service = new CtgImportService();
        } catch (Exception wse) {
            logger.error("Could not create web service: " + wse.getMessage());
            throw wse;
        }

        try {
            logger.debug("Sending offline message to MILOU");
            service.getBasicHttpBindingICtgImport().importCtg(message);
            logger.debug("Offline CTG successfully sent to MILOU server");
        } catch (SOAPFaultException e) {
            logger.error("Could not send offline message");
            logger.trace("Could not send offline message" + e.getMessage());
            throw e;
        }
        catch (Exception e){
            logger.error("Could not send offline message");
            logger.trace("Could not send offline message: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Method for sending an online CTG message to Milou Server
     * @param ctgMessage The message to send
     * @throws SOAPException
     */

    public void sendOnlineMessage(CtgMessage ctgMessage) throws SOAPException{

        RTService rtService;

        try{
            logger.debug("Instantiating MILOU web service reference.");
            rtService = new RTService();
        }
        catch (Exception e){
            logger.error("Could not create web service: " + e.getMessage());
            throw e;
        }

        try {
            logger.debug("Sending online message to MILOU");
            rtService.getBasicHttpBindingIOpenTeleRT().newMessage(ctgMessage);
            logger.debug("Online message successfully sent to MILOU");

        } catch (Exception e) {
            logger.error("Could not send SOAP message. See trace message");
            logger.trace("Could not send SOAP message: " + e.getMessage());
            throw new SOAPException("Could not send SOAP message " + e.getMessage());
        }

    }

    /**
     * Method for sending an online CTG stop message to Milou Server
     * @param modelNumber The model number of the device
     * @param registrationId The registration id of the transaction to close
     * @param stopTime The stop time
     * @throws SOAPException
     */

    public void sendOnlineStopMessage(String modelNumber, String registrationId, XMLGregorianCalendar stopTime) throws SOAPException {

        RTService rtService;

        try{
            logger.debug("Instantiating MILOU web service reference");
            rtService = new RTService();
        }
        catch (Exception e){
            logger.error("Could not create web service: " + e.getMessage());
            throw e;
        }

        try{
            logger.debug("Sending online stop message");
            rtService.getBasicHttpBindingIOpenTeleRT().stopRegistration(modelNumber, registrationId, stopTime);
            logger.debug("Online stop message successfully sent.");
        }
        catch (Exception e){
            logger.error("Could not send stop message. See trace message.");
            logger.trace("Could not send stop message: " + e.getMessage());

            throw new SOAPException("Could not send stop message " + e.getMessage());
        }

    }

}
