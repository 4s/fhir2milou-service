package dk.s4.microservices.FHIR2MilouService.Utils;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Utility class for working with date objects
 */

public class DateUtil {

    /**
     * Converts a date object to a calendar object
     * @param date The date to convert
     * @return The converted calendar object
     */

    public static Calendar date2Calendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    /**
     * Converts a calendar object to a XMLGregorianCalendar object
     * @param c The calendar object to convert
     * @return The converted XMLGregorianCalendar object
     * @throws DatatypeConfigurationException
     */

    public static XMLGregorianCalendar calendar2XMLGregorianCalendar(Calendar c) throws DatatypeConfigurationException {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(c.getTime()));
    }

    /**
     * Converts a string to a XMLGregorianCalendar object
     * @param input The string
     * @return The XMLGregorianCalendar object
     */

    public static XMLGregorianCalendar stringToXMLGregorian(String input) throws DatatypeConfigurationException, ParseException {
        Calendar calendar = stringToCalendar(input);
        return calendar2XMLGregorianCalendar(calendar);
    }

    /**
     * Converts a string to a calendar object
     * @param input The string to convert
     * @return The converted calendar object
     */

    public static Calendar stringToCalendar(String input) throws ParseException {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        Date date = format.parse(input);

        Calendar c = Calendar.getInstance();
        c.setTime(date);

        return c;

    }

}
