package dk.s4.microservices.FHIR2MilouService.Utils;

import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Utility class for validating FHIR CTG resources. Validation errors found are added to the ValidationResult object.
 */

public class CTGValidator {

    private final Logger logger = LoggerFactory.getLogger(CTGValidator.class);

    // Object holding extracted values and validation errors found
    private ValidationResult result = new ValidationResult();

    // List of required fields that must be contained in a CTG recording
    private List<String> requiredFields = new ArrayList<>();

    // Codes for CTG fields
    private static final String CTG_OBSERVATION_CODE = "8450059";
    private static final String MHR_CODE = "149546";
    private static final String FHR_CODE = "8450048";
    private static final String UA_CODE = "8450049";
    private static final String EVENT_MARKER_CODE = "8450051";
    private static final String SIGNAL_QUALITY_CODE = "8450053";
    private static final String OBSERVATION = "Observation";

    /**
     * Starts validation of a resource
     * @param resource The resource to validate
     * @return The validation result
     */

    public ValidationResult validate(IBaseResource resource){

        // Set the required fields that must be present in the CTG recording
        requiredFields.add(FHR_CODE);
        requiredFields.add(MHR_CODE);
        requiredFields.add(UA_CODE);
        requiredFields.add(SIGNAL_QUALITY_CODE);

        // Remember the resource type
        result.setResourceType(resource.getClass());

        // If resource is bundle, extract the master observation
        if (resource.getClass().equals(Bundle.class)){
            resource = findMasterObservation((Bundle) resource);
        }

        if (resource == null || !resource.getClass().equals(Observation.class)){
            result.addValidationError(ErrorMessages.MASTER_OBSERVATION_NOT_FOUND);
            return result;
        }

        // Start the validation
        validateMasterObservation((Observation) resource);

        // If not all required fields were found in the resource - add validation errors
        for (String requiredField : requiredFields) {
            result.addValidationError(String.format(ErrorMessages.MISSING_FIELD, requiredField));
        }

        return result;

    }


    /**
     * Validates the master observation resource
     * @param observation  The observation being read
     */

    private void validateMasterObservation(Observation observation) {

        // Validates that the resource has a status i.e. final or preliminary
        if (observation.hasStatus() == false){
            result.addValidationError(ErrorMessages.ONLINE_OBSERVATION_NO_STATUS);
            return;
        }

        // Remember the resource status
        result.setResourceStatus(observation.getStatus().name());

        // If the observation is an online CTG, it must contain a registration id extension
        if (result.isOnline()){
            validateRegistrationId(observation);
        }

       validateStartAndEndTime(observation);

        // Validate performer
        validatePerformer(observation);

        // The observation must contain a CTG observation code
        if (observation.hasCode() == false){
            result.addValidationError(ErrorMessages.OBSERVATION_MISSING_CODE);
            return;
        }

        CodeableConcept cc = observation.getCode();

        if (!cc.hasCoding()){
            result.addValidationError(ErrorMessages.OBSERVATION_MISSING_CODING);
            return;
        }

        Coding c = cc.getCoding().stream().filter(coding -> coding.getCode().equals(CTG_OBSERVATION_CODE)).findAny().orElse(null);

        if (c == null){
            result.addValidationError(ErrorMessages.OBSERVATION_MISSING_CODING);
            return;
        }

        // Validate contained observations
        observation.getContained().forEach(this::validateChildObservation);

    }

    /**
     * Validates CTG resources
     * @param resource The resource to validate
     */

    private void validateChildObservation(Resource resource){

        // Resource must be an observation
        if (resource.getClass().equals(Observation.class) == false){
            result.addValidationError(ErrorMessages.EXPECTED_OBSERVATION_RESOURCE);
            return;
        }

        Observation observation = (Observation) resource;

        // The observation must contain a device reference
        if (observation.hasDevice() == false){
            result.addValidationError(ErrorMessages.OBSERVATION_MISSING_DEVICE);
        }
        else{
            result.addDeviceReference(observation.getDevice());
        }

        // The observation must contain a coding
        if (!observation.hasCode()){
            result.addValidationError(ErrorMessages.OBSERVATION_MISSING_CODE);
            return;
        }

        CodeableConcept cc = observation.getCode();

        if (!cc.hasCoding()){
            result.addValidationError(ErrorMessages.OBSERVATION_MISSING_CODING);
            return;
        }

        String code = cc.getCoding().get(0).getCode();

        SampledData data;
        switch (code){
            case MHR_CODE:
                // Mark the observation as present in the recording
                markElementAsRead(code);

                data =  getSampledData(observation, code);

                // Validate sampled data
                validateSampledData(code,data, observation, "/min");
                result.setMHR(data);

                // If observation is online, validate the sequence number
                // todo this should be done generically and not just on the MHR field

                if (result.isOnline()){
                    validateSequenceNumber(observation);
                }

                break;
            case FHR_CODE:

                // Mark the observation as present in the recording
                markElementAsRead(code);

                data =  getSampledData(observation, code);


                // Validate sampled data
                validateSampledData(code, data, observation, "/min");
                result.setFHR(data);
                break;
            case UA_CODE:

                // Mark the observation as present in the recording
                markElementAsRead(code);

                data =  getSampledData(observation, code);


                // Validate sampled data
                validateSampledData(code, data, observation, "uV");
                result.setTOCO(data);
                break;
            case EVENT_MARKER_CODE:
                validateEventMarker(observation);
                break;
            case SIGNAL_QUALITY_CODE:

                // Mark the observation as present in the recording
                markElementAsRead(code);

                data =  getSampledData(observation, code);


                // Validate sampled data
                validateSampledData(code, data, observation, "1");
                result.setSQ(data);
                break;
            default:
                logger.debug(String.format("Unknown field type: %s", code));

        }

    }


    /**
     * Validates that the event marker has a valid datetime
     * @param observation The event marker observation
     */

    private void validateEventMarker(Observation observation) {

        logger.debug(String.format("Validating %s: %s", "observation", "Event marker"));

        if (!observation.hasEffectiveDateTimeType()){
            result.addValidationError(ErrorMessages.MARKER_COULD_NOT_PARSE_DATE);
            return;
        }

        if (!observation.getEffectiveDateTimeType().hasValue()){
            result.addValidationError(ErrorMessages.MARKER_COULD_NOT_PARSE_DATE);
            return;
        }

        try{
            Date date = observation.getEffectiveDateTimeType().getValue();
            result.getMarkers().add(DateUtil.date2Calendar(date));

        }
        catch (FHIRException e){
            result.addValidationError(ErrorMessages.MARKER_COULD_NOT_PARSE_DATE);
        }

    }




    private SampledData getSampledData(Observation observation, String fieldCode){
        try{
            return observation.getValueSampledData();
        }
        catch (FHIRException e){
            result.addValidationError(String.format(ErrorMessages.MISSING_SAMPLED_DATA, fieldCode));
            return null;
        }
    }


    /**
     * Validates that the sampled data read contains origin, period, factor and dimensions
     * @param fieldCode The field code being validated for printing error messages
     * @param observation The observation being validated
     */

    private void validateSampledData(String fieldCode, SampledData data, Observation observation, String expectedCode) {

        logger.debug("Validating sampled data");

        // The observation must contain sampled data
        if (observation.hasValueSampledData() == false){
            result.addValidationError(String.format(ErrorMessages.MISSING_SAMPLED_DATA, fieldCode));
        }

        // If the sampled data does not contain data, set an error
        if(!data.hasData()){
            result.addValidationError(String.format(ErrorMessages.SAMPLED_DATA_NO_DATA, fieldCode));
        }

        String dataString = data.getData();

        // split the data into an array of strings
        String[] dataValues = dataString.split("\\s+");

        // Check that all values can be cast to int
        for (String dataValue : dataValues) {
            try {
                Integer.parseInt(dataValue);
            } catch (NumberFormatException e) {
                result.addValidationError(String.format(ErrorMessages.SAMPLED_DATA_INVALID_DATA, fieldCode));
            }
        }

        // If the sampled data does not have origin, set error
        if (!data.hasOrigin()){
            result.addValidationError(String.format(ErrorMessages.SAMPLED_DATA_MISSING_ORIGIN,  fieldCode));
        }
        else{
            Quantity o = data.getOrigin();

            if (!o.hasValue()){
                result.addValidationError(String.format(ErrorMessages.SAMPLED_DATA_MISSING_ORIGIN,  fieldCode));
            }

            if (o.hasCode() == false || o.getCode().equals(expectedCode) == false){
                result.addValidationError(String.format(ErrorMessages.SAMPLED_DATA_MISSING_CODE,  fieldCode));
            }
        }

        if (!data.hasPeriod()){
            result.addValidationError(String.format(ErrorMessages.SAMPLED_DATA_MISSING_PERIOD, fieldCode));
        }

        if (!data.hasFactor()){
            result.addValidationError(String.format(ErrorMessages.SAMPLED_DATA_MISSING_FACTOR,  fieldCode));
        }

        if (!data.hasDimensions()){
            result.addValidationError(String.format(ErrorMessages.SAMPLED_DATA_MISSING_DIMENSIONS, fieldCode));
        }

    }

    /**
     * Validates that an online CTG observation has a sequence number
     * @param observation The observation to validate
     */

    private void validateSequenceNumber(Observation observation){

        logger.debug("Validating sequence number");

        // Find the sequence number extension
        Extension sequenceNumberExtension = observation.getExtensionByUrl(System.getenv("SEQUENCE_NUMBER_EXTENSION_URL"));

        // If the extension was not found, set an error
        if (sequenceNumberExtension == null){
            result.addValidationError(ErrorMessages.ONLINE_MISSING_SEQUENCE_NUMBER);
            return;
        }

        // If the extension does not have a value, set an error
        if (sequenceNumberExtension.hasValue() == false){
            result.addValidationError(ErrorMessages.ONLINE_MISSING_SEQUENCE_NUMBER);
            return;
        }

        // If the value does not have a primitive value, set an error
        if (sequenceNumberExtension.getValue().hasPrimitiveValue() == false){
            result.addValidationError(ErrorMessages.ONLINE_MISSING_SEQUENCE_NUMBER);
            return;
        }

        // Cast primitive value to int
        try{
            // set the sequence number
            result.setSequenceNumber( Integer.parseInt(sequenceNumberExtension.getValue().primitiveValue()));
        }
        catch (NumberFormatException e){
            result.addValidationError(ErrorMessages.ONLINE_SEQUENCE_NUMBER_PARSING_ERROR);
        }

    }

    /**
     * Validates that the CTG has performers
     * @param observation
     */

    private void validatePerformer(Observation observation){
        logger.debug("Validating performer");

        // The observation must contain a performer
        if (observation.hasPerformer() == false){
            result.addValidationError(ErrorMessages.CTG__MISSING_PERFORMER);
            return;
        }

        List<Reference> patientReferences = new ArrayList<>();

        // Look through the performers
        for (Reference ref : observation.getPerformer()){

            if (ref.hasIdentifier() == false || ref.getIdentifier().hasSystem() == false){
                continue;
            }

            // If the performer matches the system identifier, add to list
            if (ref.getIdentifier().getSystem().equals(System.getenv("PATIENT_IDENTIFIER_SYSTEM"))){
                patientReferences.add(ref);
            }

        }

        // If no performers found
        if (patientReferences.isEmpty()){
            result.addValidationError(ErrorMessages.CTG__MISSING_PERFORMER);
            return;
        }

        // Set patient
        result.setPatientRef(patientReferences.get(0));

    }

    /**
     * Validates that an online master observation has a registration id
     * @param observation The observation to validate
     */

    private void validateRegistrationId(Observation observation){

        logger.debug("Validating registration id");

        Extension registrationIdExtension = observation.getExtensionByUrl(System.getenv("REGISTRATION_ID_EXTENSION_URL"));

        // Check that the extension was found
        if (registrationIdExtension == null){
            result.addValidationError(ErrorMessages.REGISTRATION_ID_MISSING_FROM_ONLINE_OBSERVATION);
            return;
        }

        // Validate that the extension has value and that the value has primitive value
        if ( registrationIdExtension.hasValue() == false || registrationIdExtension.getValue().hasPrimitiveValue() == false){
            result.addValidationError(ErrorMessages.REGISTRATION_ID_MISSING_FROM_ONLINE_OBSERVATION);
            return;
        }

        // Set the registration id
        result.setRegistrationId(registrationIdExtension.getValue().primitiveValue());

    }

    /**
     * Validates that a master observation has start and end time
     * @param observation
     */

    private void validateStartAndEndTime(Observation observation){

        // The master observation must have an effective period

        if (!observation.hasEffectivePeriod()){
            result.addValidationError(ErrorMessages.MASTER_OBSERVATION_MISSING_EFFECTIVE_DATE_TYPE_TIME);
            return;
        }

        Date startDate = observation.getEffectivePeriod().getStart();
        Date endDate = observation.getEffectivePeriod().getEnd();

        // Try to convert the start and end dates
        try{
            result.setStartTime(DateUtil.date2Calendar(startDate));
            result.setEndTime(DateUtil.date2Calendar(endDate));
        }
        catch (FHIRException e){
            result.addValidationError(ErrorMessages.MASTER_OBSERVATION_MISSING_EFFECTIVE_DATE_TYPE_TIME);
        }

    }

    /**
     * Extracts a master observation from a bundle
     * @param bundle The bundle to extract the master observation from
     * @return The master observation
     */

    private Resource findMasterObservation(Bundle bundle) {

        Bundle.BundleEntryComponent entry;

        for(int i = 0; i < bundle.getEntry().size(); i++) {

            entry = bundle.getEntry().get(i);

            if (entry.isEmpty() || entry.hasResource() == false){
                continue;
            }

            if(entry.getResource().getResourceType().toString().equals(OBSERVATION)){
                return entry.getResource();
            }

        }

        return null;

    }

    /**
     * Marks a required element as being present in the bundle
     * @param name The name of the element that has been read
     */

    private void markElementAsRead(String name) {
        logger.debug(String.format("Validating %s", name));

        // Remove the element form the list of required elements
        boolean elementExisted = requiredFields.remove(name);

        // Add an error if the element has already been read
        if (!elementExisted){
            result.addValidationError(String.format(ErrorMessages.ELEMENT_DUPLICATE, name));
        }
    }

}
