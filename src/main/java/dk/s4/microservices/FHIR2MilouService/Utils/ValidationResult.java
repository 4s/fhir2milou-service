package dk.s4.microservices.FHIR2MilouService.Utils;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.SampledData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This object holds validation errors and data values found by the validation method
 * needed by the rest of the service.
 */

public class ValidationResult {
    private Reference patientRef;
    private List<Reference> deviceRefs = new ArrayList<>();
    private Calendar startTime;
    private Calendar endTime;
    private List<Calendar> markers = new ArrayList<>();
    private SampledData MHR;
    private SampledData FHR;
    private SampledData SQ;
    private SampledData TOCO;
    private String registrationId;
    private int sequenceNumber;
    private List<String> validationErrors = new ArrayList<>();
    private String resourceStatus; // either preliminary or final
    private Class resourceType;

    public void setResourceType(Class c) {
        this.resourceType = c;
    }

    public boolean isOnline() {

        boolean isBundle = resourceType.equals(Bundle.class);

        return !(isBundle && isFinal());
    }

    public void setResourceStatus(String value) {
        this.resourceStatus = value;
    }

    public String getResourceStatus() {
        return resourceStatus;
    }

    public boolean isFinal() {
        return resourceStatus.equalsIgnoreCase("final");
    }

    public boolean isPreliminary() {
        return resourceStatus.equalsIgnoreCase("preliminary");
    }

    // Patient and device references
    public List<Reference> getDeviceReferences() {
        return deviceRefs;
    }

    public void addDeviceReference(Reference device) {
        this.deviceRefs.add(device);
    }

    public Reference getPatientRef() {
        return patientRef;
    }

    public void setPatientRef(Reference patientRef) {
        this.patientRef = patientRef;
    }

    // Data values
    public Calendar getStartTime() {
        return startTime;
    }

    public void setStartTime(Calendar date) {
        this.startTime = date;
    }

    public Calendar getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Calendar endTime) {
        this.endTime = endTime;
    }

    public List<Calendar> getMarkers() {
        return markers;
    }

    public SampledData getMHR() {
        return MHR;
    }

    public void setSQ(SampledData sq) {
        this.SQ = sq;
    }

    public SampledData getSQ() {
        return SQ;
    }

    public void setMHR(SampledData MHR) {
        this.MHR = MHR;
    }

    public SampledData getFHR() {
        return FHR;
    }

    public void setFHR(SampledData FHR) {
        this.FHR = FHR;
    }

    public SampledData getTOCO() {
        return TOCO;
    }

    public void setTOCO(SampledData TOCO) {
        this.TOCO = TOCO;
    }

    // Online specific data values
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int value) {
        this.sequenceNumber = value;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String value) {
        registrationId = value;
    }

    // Errors
    public List<String> getValidationErrors() {
        return validationErrors;
    }

    public void addValidationError(String error) {
        validationErrors.add(error);
    }

    public boolean isValid() {
        return this.validationErrors.isEmpty();
    }

}
