package dk.s4.microservices.FHIR2MilouService.Utils;

import dk.s4.microservices.FHIR2MilouService.Exceptions.SampleDataException;
import dk.s4.microservices.FHIR2MilouService.Exceptions.UnitConverterException;
import org.hl7.fhir.r4.model.SampledData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPOutputStream;

public class Utility {

    private static final Logger logger = LoggerFactory.getLogger(Utility.class);

    /**
     * Compresses a string into a byte array
     *
     * @param input the string to toGZIP
     * @return a compressed byte array
     * @throws IOException
     */
    public static byte[] toGZIP(String input) throws IOException {

        ByteArrayOutputStream rstBao = new ByteArrayOutputStream();
        GZIPOutputStream zos = new GZIPOutputStream(rstBao);
        zos.write(input.getBytes());
        zos.close();

        return rstBao.toByteArray();

    }

    /**
     * Merges 4 lists into a string
     *
     * @param fhr  A list of FHR data samples
     * @param mhr  A list of MHR data samples
     * @param toco A list of TOCO data samples
     * @param sq   A list of QFHR data samples
     * @return A merged string list [ fhir1 mhr1 toco1 qfhr1 ] [ fhir2 mhr2 toco2 qfhr2 ]...
     */
    public static String mergeSampledData(List<Double> fhr, List<Double> mhr, List<Double> toco, List<Integer> sq) {
        StringBuilder result = new StringBuilder();

        // lists are already checked to be of equal length
        for (int i = 0; i < fhr.size(); i++) {
            result.append(String.format("[%s, %s, %s, %s]\n", fhr.get(i), mhr.get(i), toco.get(i), sq.get(i)));

        }
        return result.toString();
    }

    /**
     * Performs unit conversion on TOCO, FHR and MHR sampledData samples
     *
     * @param sampledData the sampledData to convert
     * @param unitFactor  the factor to be used in the conversion
     * @return A list of converted doubles.
     */
    public static ArrayList<Double> unitConverter(SampledData sampledData, double unitFactor) throws UnitConverterException {

        if (sampledData == null) {
            throw new UnitConverterException("No input sampledData");
        }

        ArrayList<Double> convertedValues = new ArrayList<>();

        if (!sampledData.hasData()) {
            return convertedValues;
        }

        String dataString = sampledData.getData();

        if (!sampledData.hasFactor() || !sampledData.hasOrigin()) {
            throw new UnitConverterException("Factor or origin not set");
        }

        String[] dataValues = dataString.split("\\s+");
        double factor = sampledData.getFactor().doubleValue();
        double origin = sampledData.getOrigin().getValue().doubleValue();

        double convertedValue;
        double parsedValue;

        for (String dataValue : dataValues) {
            try {
                parsedValue = Double.parseDouble(dataValue);
            } catch (NumberFormatException e) {
                throw new UnitConverterException("Could not parse sampled sampledData value: " + dataValue);
            }

            convertedValue = (parsedValue * factor + origin) * unitFactor;
            convertedValues.add(convertedValue);
        }
        return convertedValues;
    }

    /**
     * Manages the data conversion and returns them in an object
     *
     * @param evo The validation results of a resource
     * @return An object holding the converted data
     * @throws UnitConverterException
     * @throws DatatypeConfigurationException
     */
    public static ConvertedData performConversions(ValidationResult evo) throws UnitConverterException, DatatypeConfigurationException, SampleDataException {
        List<Integer> sqConvertedIntegers = new ArrayList<>();
        List<Double> fhrConverted;
        List<Double> mhrConverted;
        List<Double> tocoConverted;
        List<Double> sqConverted;

        try {
            fhrConverted = Utility.unitConverter(evo.getFHR(), 1);
            mhrConverted = Utility.unitConverter(evo.getMHR(), 1);

            // The TOCO values from the Monica device range between 0 and 500
            // but must be sent to the MILOU server as a range between 0 and 100 and are therefore multiplied by 0.2
            tocoConverted = Utility.unitConverter(evo.getTOCO(), 0.2);
            sqConverted = Utility.unitConverter(evo.getSQ(), 1);
        } catch (UnitConverterException e) {
            logger.error("Unit conversion could not be performed on sampled data values: ", e);
            throw e;
        }

        // lists must be of equal length to be zipped for offline and to be sent for online
        int expectedLength = fhrConverted.size();
        if (mhrConverted.size() != expectedLength || tocoConverted.size() != expectedLength || sqConverted.size() != expectedLength) {
            logger.error("Sample data length mismatch.");
            throw new SampleDataException("Sample length mismatch");
        }

        // Date conversion
        XMLGregorianCalendar contactCalendar;
        XMLGregorianCalendar endCalendar;
        XMLGregorianCalendar startCalendar;

        List<XMLGregorianCalendar> markersCalendar = new ArrayList<>();
        try {
            contactCalendar = DateUtil.calendar2XMLGregorianCalendar(evo.getStartTime());
            endCalendar = DateUtil.calendar2XMLGregorianCalendar(evo.getEndTime());
            startCalendar = DateUtil.calendar2XMLGregorianCalendar(evo.getStartTime());

            for (int i = 0; i < evo.getMarkers().size(); i++) {
                markersCalendar.add(DateUtil.calendar2XMLGregorianCalendar(evo.getMarkers().get(i)));
            }

        } catch (IllegalArgumentException | DatatypeConfigurationException e) {
            logger.error("Could not convert string to XMLGregorian calendar: " + e);
            throw e;
        }

        for (Double d : sqConverted) {
            sqConvertedIntegers.add(d.intValue());
        }

        ConvertedData convertedData = new ConvertedData();
        convertedData.setContactCalendar(contactCalendar);
        convertedData.setEndCalendar(endCalendar);
        convertedData.setFhrConverted(fhrConverted);
        convertedData.setMarkersCalendar(markersCalendar);
        convertedData.setMhrConverted(mhrConverted);
        convertedData.setStartCalendar(startCalendar);
        convertedData.setTocoConverted(tocoConverted);
        convertedData.setSqConverted(sqConvertedIntegers);

        return convertedData;
    }
}
