package dk.s4.microservices.FHIR2MilouService.Utils;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class used for storing converted values
 */

public class ConvertedData {

    private List<Double> fhrConverted;
    private List<Double> mhrConverted;
    private List<Double> tocoConverted;
    private List<Integer> sqConverted;
    private XMLGregorianCalendar contactCalendar;
    private XMLGregorianCalendar endCalendar;
    private XMLGregorianCalendar startCalendar;
    private List<XMLGregorianCalendar> markersCalendar = new ArrayList<>();

    public List<Double> getFhrConverted() {
        return fhrConverted;
    }

    public void setFhrConverted(List<Double> fhrConverted) {
        this.fhrConverted = fhrConverted;
    }

    public List<Double> getMhrConverted() {
        return mhrConverted;
    }

    public void setMhrConverted(List<Double> mhrConverted) {
        this.mhrConverted = mhrConverted;
    }

    public List<Double> getTocoConverted() {
        return tocoConverted;
    }

    public void setTocoConverted(List<Double> tocoConverted) {
        this.tocoConverted = tocoConverted;
    }

    public List<Integer> getSqConverted() {
        return sqConverted;
    }

    public void setSqConverted(List<Integer> sqConverted) {
        this.sqConverted= sqConverted;
    }

    public XMLGregorianCalendar getContactCalendar() {
        return contactCalendar;
    }

    public void setContactCalendar(XMLGregorianCalendar contactCalendar) {
        this.contactCalendar = contactCalendar;
    }

    public XMLGregorianCalendar getEndCalendar() {
        return endCalendar;
    }

    public void setEndCalendar(XMLGregorianCalendar endCalendar) {
        this.endCalendar = endCalendar;
    }

    public XMLGregorianCalendar getStartCalendar() {
        return startCalendar;
    }

    public void setStartCalendar(XMLGregorianCalendar startCalendar) {
        this.startCalendar = startCalendar;
    }

    public List<XMLGregorianCalendar> getMarkersCalendar() {
        return markersCalendar;
    }

    public void setMarkersCalendar(List<XMLGregorianCalendar> markersCalendar) {
        this.markersCalendar = markersCalendar;
    }
}
