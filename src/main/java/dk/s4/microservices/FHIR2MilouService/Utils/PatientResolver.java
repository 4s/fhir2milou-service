package dk.s4.microservices.FHIR2MilouService.Utils;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.FHIR2MilouService.Exceptions.FatalFormatException;
import dk.s4.microservices.FHIR2MilouService.Exceptions.FatalInternalErrorException;
import dk.s4.microservices.FHIR2MilouService.service.FHIR2Milou;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Contains functionality to get Patient resource from another service
 */
public class PatientResolver {
    private final static Logger logger = LoggerFactory.getLogger(PatientResolver.class);

    // URL for the patient-service
    private String patientServiceUrl;

    // Cache for patient resources
    private Map<String, Patient> cachedPatients = new ConcurrentHashMap<>();

    public PatientResolver(String url) {
        patientServiceUrl = url;
    }

    /**
     * From a full URL reference to a patient resource get the patient resource
     *
     * @param patientReference Full url reference to a patient
     * @return A patient resource.
     * @throws ResourceNotFoundException
     */
    public Patient getPatient(Reference patientReference) throws ResourceNotFoundException, FatalFormatException, FatalInternalErrorException {
        Identifier identifier = patientReference.getIdentifier();

        if (identifier.hasSystem() == false || identifier.hasValue() == false) {
            throw new FatalFormatException("Patient identifier with system and value not found");
        }

        IGenericClient patientClient = MyFhirClientFactory.getInstance().getClientFromBaseUrl(
                patientServiceUrl,
                FHIR2Milou.getFhirContext());

        String searchUrl = patientServiceUrl
                + "/Patient?_query=searchByIdentifier&identifier="
                + identifier.getSystem()
                + "|" + identifier.getValue();
        Bundle bundle = patientClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

        if (!bundle.hasEntry() || bundle.getEntry().isEmpty())
            throw new FatalInternalErrorException("Failed patient lookup");

        if (!bundle.getEntry().get(0).hasResource() || bundle.getEntry().get(0).getResource().isEmpty()) {
            throw new FatalInternalErrorException("Found entry with no resource - should not happen");
        }

        Patient lookedUpPatient = (Patient) bundle.getEntry().get(0).getResource();
        if (!lookedUpPatient.hasIdentifier()) {
            throw new FatalInternalErrorException("Found patient with no identifier - should not happen");
        }
        return lookedUpPatient;
    }

    /**
     * Removes a patient from the cache
     *
     * @param key The guid of the patient to remove
     */

    public void removePatient(String key) {
        logger.debug("Removing patient from cache.");
        Object removedPatient = cachedPatients.remove(key);
        if (removedPatient != null) {
            logger.debug("Patient removed.");
        } else {
            logger.debug("Could not remove patient from cache. Patient not.");
        }
    }

    /**
     * Finds a patient in the cache
     *
     * @param key The key the patient was chaced under
     * @return The patient if found, null otherwise
     */

    public Patient findPatientInCache(String key) {
        if (key == null) {
            logger.debug("Could not look in cache. Guid was null.");
            return null;
        }
        logger.debug("Looking in cache");
        if (cachedPatients.containsKey(key)) {
            logger.debug("Patient found in cache");
            return cachedPatients.get(key);
        } else {
            logger.debug("Patient not found in cache");
            return null;
        }
    }

    /**
     * Adds a patient to the cache
     *
     * @param patient The patient to cache
     * @param guid    The key to cache the patient under
     */
    public void addPatientToCache(Patient patient, String guid) {
        logger.debug("Adding patient to cache under key: " + guid);
        cachedPatients.put(guid, patient);

        // Set a timeout to remove the patient from cache.
        // This is needed if the service never receives a stop message on online CTGs.
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                logger.debug("Patient removed from cache by timeout");
                cachedPatients.remove(guid);
            }
        }, Integer.parseInt(System.getenv("CLEAR_CACHE_TIME_OUT")));
    }
}

