package dk.s4.microservices.FHIR2MilouService.Utils;

public class ErrorMessages {

    public final static String MISSING_FIELD = "The resource did not contain: %s";

    public final static String SAMPLED_DATA_NO_DATA = "Sampled data object did not contain any data: %s";

    public final static String SAMPLED_DATA_INVALID_DATA = "Data values could not be parsed to integer for: %s";

    public final static String SAMPLED_DATA_MISSING_ORIGIN = "Origin is missing in: %s";

    public final static String SAMPLED_DATA_MISSING_PERIOD = "Period is missing in: %s";

    public final static String SAMPLED_DATA_MISSING_FACTOR = "Factor is missing in: %s";

    public final static String SAMPLED_DATA_MISSING_DIMENSIONS = "Dimensions is missing in: %s";

    public final static String ELEMENT_DUPLICATE = "Element appears more than once in the bundle: %s";

    public final static String CTG__MISSING_PERFORMER = "CTG observation did not contain performer";

    public final static String MARKER_COULD_NOT_PARSE_DATE = "Invalid effectiveDateTime in EventMarker";

    public final static String MASTER_OBSERVATION_MISSING_EFFECTIVE_DATE_TYPE_TIME = "Could not read effective date type time on master observation";

    public final static String OBSERVATION_MISSING_CODE = "Observation did not contain code";

    public final static String OBSERVATION_MISSING_CODING = "Observation did not contain coding";

    public final static String MISSING_SAMPLED_DATA = "Could not read sampled data on: %s";

    public final static String ONLINE_OBSERVATION_NO_STATUS = "Online observation does not contain status";

    public final static String ONLINE_MISSING_SEQUENCE_NUMBER = "MHR observation did not contain sequence number";

    public final static String ONLINE_SEQUENCE_NUMBER_PARSING_ERROR = "Could not parse sequence number on online observation";

    public final static String EXPECTED_OBSERVATION_RESOURCE = "Expected resource type was observation";

    public final static String REGISTRATION_ID_MISSING_FROM_ONLINE_OBSERVATION = "Online CTG did not contain a registration id";

    public final static String SAMPLED_DATA_MISSING_CODE = "Sampled data origin missing code : %s";

    public final static String OBSERVATION_MISSING_DEVICE = "Observation did not contain a device";

    public final static String MASTER_OBSERVATION_NOT_FOUND = "Master observation not found";
}