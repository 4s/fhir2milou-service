package dk.s4.microservices.FHIR2MilouService.Utils;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.FHIR2MilouService.Exceptions.FatalFormatException;
import dk.s4.microservices.FHIR2MilouService.Exceptions.FatalInternalErrorException;
import dk.s4.microservices.FHIR2MilouService.service.FHIR2Milou;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;
import io.vavr.CheckedFunction0;
import io.vavr.CheckedFunction1;
import io.vavr.control.Try;
import java.io.IOException;
import java.sql.Ref;
import java.time.Duration;
import java.util.function.Supplier;
import javax.xml.ws.WebServiceException;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Contains functionality to get Device resource from another service
 */

public class DeviceResolver {

    // URL to the device-service
    private String deviceServiceUrl;

    // Cache for device resources
    private Map<String, Device> cachedDevices = new ConcurrentHashMap<>();

    private final static Logger logger = LoggerFactory.getLogger(DeviceResolver.class);

    public DeviceResolver(String url) {
        deviceServiceUrl = url;
    }

    /**
     * From a full URL reference to a device resource get the device resource
     *
     * @param deviceReference Full url reference to a device
     * @return A device resource.
     * @throws ResourceNotFoundException
     */

    public Device getDevice(Reference deviceReference) throws ResourceNotFoundException, FatalFormatException, FatalInternalErrorException {
        int retries = Integer.parseInt(System.getenv("DEVICE_RESOLVER_RETRIES"));
        int wait = Integer.parseInt(System.getenv("DEVICE_RESOLVER_RETRY_WAIT_MS"));

        RetryConfig config = RetryConfig.custom()
                                        .maxAttempts(retries)
                                        .waitDuration(Duration.ofMillis(wait))
                                        .retryOnResult(result -> result == null)
                                        .build();

        Retry retry = Retry.of("getDevice", config);
        CheckedFunction0<Device> retriableDoGetDevice = () -> doGetDevice(deviceReference);
        retriableDoGetDevice = Retry.decorateCheckedSupplier(retry, retriableDoGetDevice);
        Device lookedUpDevice = Try.of(retriableDoGetDevice).get();

        if (lookedUpDevice == null)
            throw new FatalInternalErrorException("No matching Device could not be looked up at " + deviceServiceUrl);

        return lookedUpDevice;

    }

    private Device doGetDevice(Reference deviceReference) throws ResourceNotFoundException, FatalFormatException, FatalInternalErrorException {
        Identifier identifier = deviceReference.getIdentifier();

        logger.debug("doGetDevice");

        if (identifier.hasSystem() == false || identifier.hasValue() == false) {
            throw new FatalFormatException("Device identifier with system and value not found");
        }

        IGenericClient deviceClient = MyFhirClientFactory.getInstance().getClientFromBaseUrl(
                deviceServiceUrl,
                FHIR2Milou.getFhirContext());

        String searchUrl = deviceServiceUrl
                + "/Device?_query=getByIdentifier&identifier="
                + identifier.getSystem()
                + "|" + identifier.getValue();
        Bundle bundle = deviceClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

        Device lookedUpDevice = null;
        if (bundle.hasEntry() && !bundle.getEntry().isEmpty()) {
            if (!bundle.getEntry().get(0).hasResource() || bundle.getEntry().get(0).isEmpty()) {
                throw new FatalInternalErrorException("Found entry with no resource");
            }

            lookedUpDevice = (Device) bundle.getEntry().get(0).getResource();
            if (!lookedUpDevice.hasIdentifier())
                throw new FatalInternalErrorException("Found device with no identifier - should not happen");
        }

        return lookedUpDevice;
    }

    /**
     * Finds a device from cache
     *
     * @param key The key the device was cached under
     * @return The device object if found, null otherwise
     */

    public Device findDeviceFromCache(String key) {
        if (key == null) {
            logger.debug("Could not look in cache. Guid was null.");
            return null;
        }
        logger.debug("Looking for device in cache");
        if (cachedDevices.containsKey(key)) {
            logger.debug("Device found in cache");
            return cachedDevices.get(key);
        }
        return null;
    }

    /**
     * Adds a device to the cache
     *
     * @param device The device to cache
     * @param key    The key to cache the device under
     */
    public void addDeviceToCache(Device device, String key) {
        logger.debug("Adding device to cache");
        cachedDevices.put(key, device);

        // Schedule removal of device from cache
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                logger.debug("Removing device from cache by timeout");
                cachedDevices.remove(key);
            }
        }, Integer.parseInt(System.getenv("CLEAR_CACHE_TIME_OUT")));
    }


    /**
     * Removes a device from the cache
     *
     * @param key The key of the device to remove
     */
    public void removeFromCache(String key) {
        logger.debug("Removing device from cache.");
        if (cachedDevices.remove(key) != null) {
            logger.debug("Remove success.");
        } else {
            logger.debug("Remove failed. Device not found in cache");
        }
    }
}
