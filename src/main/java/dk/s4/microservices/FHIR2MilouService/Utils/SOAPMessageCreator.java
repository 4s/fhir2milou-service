package dk.s4.microservices.FHIR2MilouService.Utils;

import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfdateTime;
import milouWsdlCode.ObjectFactory;
import milouWsdlCode.Registration;
import milouWsdlCode.RegistrationContact;
import milouWsdlCode.RegistrationLocation;
import milouWsdlCode.RegistrationLocationClinic;
import milouWsdlCode.RegistrationLocationRoom;
import milouWsdlCode.RegistrationLocationWard;
import milouWsdlCode.RegistrationPatient;
import milouWsdlCode.RegistrationPatientName;
import org.datacontract.schemas._2004._07.milou_server.ArrayOfCtgMessageBlock;
import org.datacontract.schemas._2004._07.milou_server.CtgMessage;
import org.datacontract.schemas._2004._07.milou_server.CtgMessageBlock;
import org.datacontract.schemas._2004._07.milou_server.CtgMessagePatient;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Patient;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.DateTimeException;
import java.util.*;

/**
 * Helper class for creating SOAP messages
 */

public class SOAPMessageCreator {

    /**
     * Constructs an offline SOAP message
     * @param patient the patient
     * @param contactTimeField datetime
     * @param endTime the end time for the CTG
     * @param startTime the start time for the CTG
     * @param device the device
     * @param markers a list of markers
     * @param ctgSampledDataValues the merged CTG data sampled values mhr, fhr, toco, qfhr as a string
     * @return the constructed SOAP message
     */

    public static Registration createOfflineMessage(Patient patient,
                                                    XMLGregorianCalendar contactTimeField, XMLGregorianCalendar endTime,
                                                    XMLGregorianCalendar startTime, Device device,
                                                    List<XMLGregorianCalendar> markers, byte[] ctgSampledDataValues)
    {

        ObjectFactory objectFactory = new ObjectFactory();
        Registration registration = objectFactory.createRegistration();

        // Patient

        String patientFirstName = patient.getNameFirstRep().getGivenAsSingleString();
        String patientLastName = patient.getNameFirstRep().getFamily();
        String patientId = patient.getIdentifier().get(0).getValue();

        RegistrationPatientName patientName = new RegistrationPatientName();
        patientName.setFirstField(patientFirstName);
        patientName.setLastField(patientLastName);

        RegistrationPatient registrationPatient = objectFactory.createRegistrationPatient();
        registrationPatient.setIdField(patientId);
        registrationPatient.setNameField(patientName);

        // Contact

        RegistrationContact contact = objectFactory.createRegistrationContact();
        contact.setParityField(objectFactory.createRegistrationContactParity());
        contact.setGestationField(objectFactory.createRegistrationContactGestation());
        contact.setTimeField(contactTimeField);

        // Registration

        registration.setStartTimeField(startTime);
        registration.setEndTimeField(endTime);
        registration.setContactField(contact);
        registration.setPatientField(registrationPatient);

        // Markers

        milouWsdlCode.ArrayOfdateTime markersArrayOfDate = new milouWsdlCode.ArrayOfdateTime();
        markersArrayOfDate.getDateTime().addAll(markers);
        registration.setMarkersField(markersArrayOfDate);


        // Other
        registration.setDeviceIDField(device.getModelNumber() + " " + device.getSerialNumber());
        registration.setCtgField(ctgSampledDataValues);
        registration.setTocoShiftField(0); // TODO MONICA specific: hardcoded to zero as the value is not sent from the device

        // Location

        RegistrationLocation location = objectFactory.createRegistrationLocation();
        RegistrationLocationClinic clinic = objectFactory.createRegistrationLocationClinic();
        location.setClinicField(clinic);
        RegistrationLocationRoom room = objectFactory.createRegistrationLocationRoom();
        room.setNameField("Home");
        location.setRoomField(room);
        RegistrationLocationWard ward = objectFactory.createRegistrationLocationWard();
        ward.setNameField("");
        location.setWardField(ward);
        registration.setLocationField(location);

        return registration;
    }


    /**
     * Constructs an online SOAP message
     * @param ctgPatient The patient
     * @param res Extracted data from the validator
     * @param convertedData the data
     * @return An online Ctgmessage object
     */

    public static CtgMessage createOnlineMessage (CtgMessagePatient ctgPatient, ValidationResult res,
                                            ConvertedData convertedData,
                                            Device device) throws DateTimeException, DatatypeConfigurationException{

        ArrayOfCtgMessageBlock messageBlock = new ArrayOfCtgMessageBlock();

        int sequenceNumber = res.getSequenceNumber();

        List<Double> fhr = convertedData.getFhrConverted();
        List<Double> mhr = convertedData.getMhrConverted();
        List<Double> toco = convertedData.getTocoConverted();
        List<Integer> sq = convertedData.getSqConverted();

        XMLGregorianCalendar xmlGregorianCalendar;

        // creates blocks of 4 samples. Input data is multiples of four.
        for (int i = 0; i <fhr.size(); i = i + 4){

            sequenceNumber++;

            CtgMessageBlock block = new CtgMessageBlock();
            block.setFhrField(String.format("[%s, %s, %s, %s]", fhr.get(i), fhr.get(i + 1), fhr.get(i + 2), fhr.get(i + 3)));
            block.setMhrField(String.format("[%s, %s, %s, %s]", mhr.get(i), mhr.get(i + 1), mhr.get(i + 2), mhr.get(i + 3)));
            block.setTocoField(String.format("[%s, %s, %s, %s]", toco.get(i), toco.get(i + 1), toco.get(i + 2), toco.get(i + 3)));
            block.setSqField(String.format("[%s, %s, %s, %s]", sq.get(i), sq.get(i + 1), sq.get(i + 2), sq.get(i + 3)));

            // set the block time. Increment the block time by one second (the first block is zero seconds)
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(res.getStartTime().getTime());
            calendar.add(Calendar.SECOND, i / 4);

            try {
                xmlGregorianCalendar = DateUtil.calendar2XMLGregorianCalendar(calendar);
            }
            catch (IllegalArgumentException e) {
                throw new DateTimeException("Could not parse start time " + e.getMessage());
            }

            block.setTimeField(xmlGregorianCalendar);
            block.setSequenceNbrField(sequenceNumber);
            messageBlock.getCtgMessageBlock().add(block);

        }

        // convert and add maker dates
        ArrayOfdateTime markers = new ArrayOfdateTime();

        for (Calendar c : res.getMarkers()){
            try{
                markers.getDateTime().add(DateUtil.calendar2XMLGregorianCalendar(c));
            }
            catch (IllegalArgumentException | DatatypeConfigurationException e){
                throw new DateTimeException("Could not parse marker times" + e.getMessage());
            }
        }

        CtgMessage ctgMessage = new CtgMessage();
        ctgMessage.setDeviceIDField(device.getModelNumber() + " " + device.getSerialNumber());
        ctgMessage.setTocoShiftField(0); // hardcoded to zero as the value is not sent from the device
        ctgMessage.setRegistrationIDField(res.getRegistrationId());
        ctgMessage.setPatientField(ctgPatient);
        ctgMessage.setCtgField(messageBlock);
        ctgMessage.setMarkersField(markers);
        return ctgMessage;
    }
}