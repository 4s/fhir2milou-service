package dk.s4.microservices.FHIR2MilouService.processing;

import ca.uhn.fhir.context.ConfigurationException;
import ca.uhn.fhir.parser.DataFormatException;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.FHIR2MilouService.Exceptions.*;
import dk.s4.microservices.FHIR2MilouService.Utils.*;
import dk.s4.microservices.FHIR2MilouService.service.FHIR2Milou;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import javax.xml.ws.soap.SOAPFaultException;
import milouWsdlCode.Registration;
import org.datacontract.schemas._2004._07.milou_server.CtgMessage;
import org.datacontract.schemas._2004._07.milou_server.CtgMessagePatient;
import org.datacontract.schemas._2004._07.milou_server.CtgMessagePatientName;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;
import javax.xml.ws.WebServiceException;
import java.io.IOException;
import java.time.DateTimeException;
import java.util.List;

public class MyEventProcessor implements EventProcessor {

    private final static Logger logger = LoggerFactory.getLogger(MyEventProcessor.class);
    private IParser parser = null;

    private void setKafkaErrorResponse(Topic messageProcessedTopic,
                                       Message receivedMessage,
                                       Topic consumedTopic,
                                       Message outgoingMessage,
                                       String errorMsg) {
        messageProcessedTopic
                .setOperation(Topic.Operation.ProcessingFailed)
                .setDataCategory(consumedTopic.getDataCategory())
                .setDataType(consumedTopic.getDataType())
                .setDataCodes(consumedTopic.getDataCodes());

        logger.debug("Writing to Kafka on topic: {}", messageProcessedTopic.toString());
        logger.error(errorMsg);

        OperationOutcome outcome = new OperationOutcome();
        outcome.addIssue()
                .setSeverity(OperationOutcome.IssueSeverity.ERROR)
                .setDetails(new CodeableConcept().
                        setText(errorMsg));
        outgoingMessage
                .setBody(parser.encodeResourceToString(outcome))
                .setBodyType(receivedMessage.getBodyType())
                .setContentVersion(receivedMessage.getContentVersion())
                .setCorrelationId(receivedMessage.getCorrelationId())
                .setTransactionId(receivedMessage.getTransactionId());
    }

    private void setKafkaSuccessResponse(Topic messageProcessedTopic,
                                         Message receivedMessage,
                                         Topic consumedTopic,
                                         Message outgoingMessage,
                                         String msg) {

        messageProcessedTopic
                .setOperation(Topic.Operation.DataValidated)
                .setDataCategory(consumedTopic.getDataCategory())
                .setDataType(consumedTopic.getDataType())
                .setDataCodes(consumedTopic.getDataCodes());

        logger.debug("Writing to Kafka on topic: {}", messageProcessedTopic.toString());
        logger.info(msg);

        OperationOutcome outcome = new OperationOutcome();
        outcome.addIssue()
                .setSeverity(OperationOutcome.IssueSeverity.INFORMATION)
                .setDetails(new CodeableConcept().
                        setText(msg));

        outgoingMessage
                .setBody(parser.encodeResourceToString(outcome))
                .setBodyType(receivedMessage.getBodyType())
                .setContentVersion(receivedMessage.getContentVersion())
                .setCorrelationId(receivedMessage.getCorrelationId())
                .setTransactionId(receivedMessage.getTransactionId());
    }

    @Override
    public boolean processMessage(Topic consumedTopic,
                                  Message receivedMessage,
                                  Topic messageProcessedTopic,
                                  Message outgoingMessage) {

        // Parse resource
        parser = FHIR2Milou.getFhirContext().newJsonParser();
        IBaseResource resource;

        logger.info("Processing resource");
        try {
            resource = parser.parseResource(receivedMessage.getBody());
            logger.info("Parsing resource success");
        } catch (DataFormatException | ConfigurationException e) {
            setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Parsing CTG resource failed: " + e.getMessage());
            return false;
        }

        // Validate resource
        CTGValidator validator = new CTGValidator();

        // The validator also extracts data to the ValidationResult object
        ValidationResult validationResult = validator.validate(resource);

        if (!validationResult.isValid()) {
            String errorMsg = "Resource contained validation errors.";
            logger.error(errorMsg);
            validationResult.getValidationErrors().forEach(vr -> logger.error("Validation error:" + vr));
            setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, errorMsg);
            return false;
        }

        logger.info("Resource validated successfully.");

        // Unit convert data
        ConvertedData convertedData;

        try {
            convertedData = Utility.performConversions(validationResult);
            logger.info("Unit conversion completed successfully.");
        } catch (UnitConverterException e) {
            setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Could not perform unit conversion." + e.getMessage());
            return false;
        } catch (DatatypeConfigurationException e) {
            setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Could not parse dates. " + e.getMessage());
            return false;
        } catch (SampleDataException e) {
            setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Could not process sample data. " + e.getMessage());
            return false;
        }

        CtgType ctgType;
        try {
            ctgType = CtgType.from(resource, validationResult.isFinal());
        } catch (RuntimeException e) {
            setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, e.getMessage());
            return false;
        }

        // Handle patient lookup
        PatientResolver patientResolver = FHIR2Milou.getPatientResolver();

        // Only online CTGs have a registration Id.
        String guid = validationResult.getRegistrationId();

        Patient patient = patientResolver.findPatientInCache(guid);

        // if not found in cache or resource is offline CTG (guid is null)

        if (patient != null) {
            logger.debug("Patient found in cache");
        } else {
            logger.debug("Patient not found in cache");
            try {
                // Look up patient from patient-service

                logger.debug("Looking up patient " + validationResult.getPatientRef());

                patient = patientResolver.getPatient(validationResult.getPatientRef());

                if (patient == null) {
                    setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Patient not found: " + validationResult.getPatientRef());
                    return false;
                } else if (!patient.hasIdentifier() || patient.getIdentifier().isEmpty() || !patient.getIdentifier().get(0).hasValue()) {
                    setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Patient did not have a identifier or the identifier did not have a value");
                    return false;
                }

                // add patient to cache
                logger.trace("Adding patient to cache " + patient.getNameFirstRep());

                if (guid != null) {
                    patientResolver.addPatientToCache(patient, guid);
                }

            } catch (ResourceNotFoundException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Patient resource not found: " + e.getMessage());
                return false;
            } catch (FhirClientConnectionException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Error connecting to patient service: " + e.getMessage());
                return false;
            } catch (FatalInternalErrorException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Could not look up patient: " + e.getMessage());
                return false;
            } catch (FatalFormatException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Patient resource error: " + e.getMessage());
                return false;
            }
        }


        // Handle device lookup
        DeviceResolver deviceResolver = FHIR2Milou.getDeviceResolver();

        if (validationResult.getDeviceReferences().size() == 0){
            setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "No device references found in bundle");
            return false;
        }

        try {
            validate(validationResult.getDeviceReferences());
        } catch(IllegalArgumentException e) {
            setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Device reference invalid: " + e.getMessage());
            return false;
        }

        // we check if the device has been cached
        Device device = deviceResolver.findDeviceFromCache(guid);

        if (device == null) {
            logger.debug("Device not found in cache");

            try {
                // lookup device from device-service
                device = deviceResolver.getDevice(validationResult.getDeviceReferences().get(0));

                // add device to cache if online
                if (ctgType.isOnline() && guid != null) {
                    deviceResolver.addDeviceToCache(device, guid);
                }

            } catch (ResourceNotFoundException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Device resource not found: " + e.getMessage());
                return false;
            } catch (FhirClientConnectionException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Error connecting to device service: " + e.getMessage());
                return false;
            } catch (FatalInternalErrorException | FatalFormatException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Could not get device" + e.getMessage());
                return false;
            }

        }

        // send CTG recording
        if (ctgType.isOnline()) {
            try {
                logger.info("Preparing to send online CTG recording to MILOU-server");
                sendOnlineCTG(validationResult, convertedData, device, patient);
                setKafkaSuccessResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Success");

                if (validationResult.isFinal()) {
                    // This is the last online record, cleaning patient and device from cache
                    logger.info("Removing patient and device from cache");
                    deviceResolver.removeFromCache(validationResult.getRegistrationId());
                    patientResolver.removePatient(validationResult.getRegistrationId());
                }
                return true;
            } catch (SampleDataException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Could not send SOAP message " + e.getMessage());
                return false;
            } catch (SOAPException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Could not send SOAP message. ");
                return false;
            }
        } else {
            try {
                logger.info("Preparing to send offline CTG recording to MILOU-server");
                sendOfflineCTG(patient, device, convertedData);
                setKafkaSuccessResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "CTG sent to Milou");
                logger.debug("CTGToMilou_FHIR_Observation"); // needed for the test to pass
                return true;
            } catch (MergeException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "CTG sampled data could not be merged: " + e.getMessage());
                return false;
            } catch (IOException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Zipping sampled data failed: " + e.getMessage());
                return false;
            } catch (SOAPFaultException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Error in SOAP message." + e.getFault());
                return false;
            } catch (WebServiceException e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Could not construct CtgImportService: " + e.getCause().getMessage());
                return false;
            } catch (Exception e) {
                setKafkaErrorResponse(messageProcessedTopic, receivedMessage, consumedTopic, outgoingMessage, "Unknown error. Could not send CTG to MILOU-server " + e.getCause());
                return false;
            }
        }
    }

    protected void validate(List<Reference> references) throws IllegalArgumentException {
        for (Reference reference : references) {
            if (reference.hasIdentifier() == false) {
                throw new IllegalArgumentException("Device reference did not contain identifier");
            }
            if (reference.getIdentifier().hasSystem() == false){
                throw new IllegalArgumentException("Device reference identifier did not contain system");
            }
            if (reference.getIdentifier().hasValue() == false){
                throw new IllegalArgumentException("Device reference identifier did not contain value");
            }
        }

        String system = references.get(0).getIdentifier().getSystem();
        for (Reference reference : references) {
            if (reference.getIdentifier().getSystem().equals(system) == false) {
                throw new IllegalArgumentException("Un-equal systems: " + reference.getIdentifier().getSystem() + " - " + system);
            }
        }
    }

    private void sendOfflineCTG(Patient patient,
                                Device device,
                                ConvertedData convertedData) throws Exception {

        logger.info("Merging data for offline CTG recording");

        String merged = Utility.mergeSampledData(convertedData.getFhrConverted(), convertedData.getMhrConverted(),
                convertedData.getTocoConverted(), convertedData.getSqConverted());

        logger.info("Zipping data for offline CTG recording");

        byte[] zipped = Utility.toGZIP(merged);


        Registration message;
        try {
            message = SOAPMessageCreator.createOfflineMessage(patient, convertedData.getContactCalendar(),
                    convertedData.getEndCalendar(), convertedData.getStartCalendar(),
                    device, convertedData.getMarkersCalendar(), zipped);
        } catch (Exception e) {
            logger.error("Could not create SOAP message");
            logger.trace("Could not create SOAP message: " + e);
            return;
        }

        logger.info("Sending SOAP message for offline CTG to MILOU-server");
        MilouServerHandler milouServer = FHIR2Milou.getMilouResolver();
        milouServer.sendOfflineMessage(message);
    }


    private void sendOnlineCTG(ValidationResult validationResult,
                               ConvertedData convertedData,
                               Device device,
                               Patient patient) throws SampleDataException, SOAPException {


        // verify converted data specific to online CTG
        if (convertedData.getFhrConverted().size() % 4 != 0) {
            logger.error("Wrong length of data. Must be a multiple of four for online CTG.");
            throw new SampleDataException("Wrong length of data. Must be a multiple of four for online CTG.");
        }

        // set patient info
        CtgMessagePatientName name = new CtgMessagePatientName();
        name.setFirstField(patient.getNameFirstRep().getGivenAsSingleString());
        name.setLastField(patient.getNameFirstRep().getFamily());
        CtgMessagePatient ctgPatient = new CtgMessagePatient();
        ctgPatient.setIdField(patient.getIdentifier().get(0).getValue());
        ctgPatient.setNameField(name);


        CtgMessage ctgMessage;
        try {
            ctgMessage = SOAPMessageCreator.createOnlineMessage(ctgPatient, validationResult, convertedData, device);
        } catch (DateTimeException | DatatypeConfigurationException e) {
            logger.error("Could not create online SOAP message");
            throw new SOAPException("Could not create online SOAP message " + e.getMessage());
        }

        MilouServerHandler msh = FHIR2Milou.getMilouResolver();
        msh.sendOnlineMessage(ctgMessage);
        logger.debug("Online CTG successfully sent ");
        logger.debug("CTG_Online_Observation"); // needed for test to pass

        if (validationResult.isPreliminary()) {
            return;
        }

        // if final, send stop message to the MILOU-SERVER
        XMLGregorianCalendar xmlGregorianCalendar;
        try {
            xmlGregorianCalendar = DateUtil.calendar2XMLGregorianCalendar(validationResult.getEndTime());
            logger.info("Stop time: " + xmlGregorianCalendar);
        } catch (IllegalArgumentException | DatatypeConfigurationException e) {
            logger.error("Could not parse end time for stop message.");
            throw new DateTimeException("Could not parse end time " + e.getMessage());
        }
        logger.info("Sending stop message to the MILOU-SERVER");
        try {
            msh.sendOnlineStopMessage(device.getModelNumber(),
                    validationResult.getRegistrationId(), xmlGregorianCalendar);
            logger.debug("CTG_Online_Stop_Observation"); // needed for test to pass

        } catch (SOAPException e) {
            logger.error("Could not send stop message.");
            throw e;
        }
    }
}
