package dk.s4.microservices.FHIR2MilouService.processing;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.instance.model.api.IBaseResource;


public enum CtgType {
    OFFLINE_BUNDLE(false),
    ONLINE_BUNDLE(true),
    ONLINE_OBSERVATION(true);

    private final boolean isOnline;

    CtgType(boolean isOnline) {
        this.isOnline = isOnline;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public static CtgType from(IBaseResource resource, boolean isFinal) {
        if (resource.getClass().equals(Bundle.class)) {
            if(isFinal) {
                return OFFLINE_BUNDLE;
            } else {
                return ONLINE_BUNDLE;
            }
        }
        if (resource.getClass().equals(Observation.class)) {
            return ONLINE_OBSERVATION;
        }
        throw new RuntimeException("Unknown resource type");
    }
}
