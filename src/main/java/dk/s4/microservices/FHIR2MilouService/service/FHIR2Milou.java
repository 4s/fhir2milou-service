package dk.s4.microservices.FHIR2MilouService.service;

import static net.logstash.logback.argument.StructuredArguments.kv;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.StrictErrorHandler;
import dk.s4.microservices.FHIR2MilouService.Utils.DeviceResolver;
import dk.s4.microservices.FHIR2MilouService.Utils.MilouServerHandler;
import dk.s4.microservices.FHIR2MilouService.Utils.PatientResolver;
import dk.s4.microservices.FHIR2MilouService.Utils.ServiceUtils;
import dk.s4.microservices.FHIR2MilouService.processing.MyEventProcessor;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.microservicecommon.Env;
import dk.s4.microservices.microservicecommon.MetricsEventConsumerInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.security.DiasEventConsumerInterceptor;
import dk.s4.microservices.microservicecommon.security.DiasUserContextResolver;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The FHIR2Milou-service subscribes to Kafka topics to receive new offline and online CTG recordings. The
 * recordings will be validated, data will be converted and the result will be sent to the MILOU SERVER.
 */

public class FHIR2Milou {

	private static FhirContext fhirContext;

	public static Runnable kafkaConsumeAndProcessThread;

	public static List<Topic> topics;

	public static FhirContext getFhirContext(){
		if (fhirContext == null){
		 	fhirContext = FhirContext.forR4();
		 	fhirContext.setParserErrorHandler(new StrictErrorHandler());
		}
		return fhirContext;
	}

	private final static Logger logger = LoggerFactory.getLogger(FHIR2Milou.class);

	//The following to be able to inject a mock PatientResolver from test code
	public static PatientResolver patientResolver = null;
	public static PatientResolver getPatientResolver() {
		if (patientResolver == null){
			String url = System.getenv("PATIENTCARE_SERVICE_URL");
			patientResolver = new PatientResolver(url);
		}
		return patientResolver;

	}

	// The following to be able to inject a mock DeviceResolver from test code
	public static DeviceResolver deviceResolver = null;
	public static DeviceResolver getDeviceResolver() {
		if (deviceResolver == null){
			String url = System.getenv("OUTCOME_SERVICE_URL");
			deviceResolver = new DeviceResolver(url);
		}
		return deviceResolver;
	}

	//The following to be able to inject a mock MilouResolver from test code
	public static MilouServerHandler milouResolver = null;
	public static MilouServerHandler getMilouResolver() {
		if (milouResolver == null) {
			milouResolver = new MilouServerHandler();
		}
		return milouResolver;
	}

	private static void registerRequiredEnvVars(){

		Env.registerRequiredEnvVars(Arrays.asList(
				"INTEGRATION_TEST",
				"PATIENTCARE_SERVICE_URL",
				"OUTCOME_SERVICE_URL",
				"PATIENT_IDENTIFIER_SYSTEM",
				"CLEAR_CACHE_TIME_OUT",
				"REGISTRATION_ID_EXTENSION_URL",
				"SEQUENCE_NUMBER_EXTENSION_URL",
				"MOCK_PATIENT_RESOLVER",
				"MOCK_DEVICE_RESOLVER",
				"ENABLE_HEALTH",
				"HEALTH_FILE_PATH",
				"HEALTH_INTERVAL_MS",
				"ENABLE_KAFKA",
				"PROMETHEUS_PUSHGATEWAY",
				"LOG_LEVEL",
				"LOG_LEVEL_DK_S4",
				"DEVICE_RESOLVER_RETRIES",
				"DEVICE_RESOLVER_RETRY_WAIT_MS"
				));

		Env.registerConditionalEnvVars("MOCK_PATIENT_RESOLVER", Arrays.asList(
				"MOCK_PATIENT_FIRST_NAME",
				"MOCK_PATIENT_LAST_NAME",
				"MOCK_PATIENT_CPR",
				""
		));

		Env.registerConditionalEnvVars("MOCK_DEVICE_RESOLVER", Collections.singletonList(
				"MOCK_DEVICE_MODEL_NUMBER"
		));


		Env.registerConditionalEnvVars("ENABLE_KAFKA", Arrays.asList(
				"KAFKA_BOOTSTRAP_SERVER",
				"KAFKA_KEY_DESERIALIZER",
				"KAFKA_VALUE_DESERIALIZER",
				"KAFKA_ENABLE_AUTO_COMMIT",
				"KAFKA_AUTO_COMMIT_INTERVAL_MS",
				"KAFKA_SESSION_TIMEOUT_MS",
				"KAFKA_GROUP_ID",
				"KAFKA_ACKS",
				"KAFKA_RETRIES",
				"KAFKA_KEY_SERIALIZER",
				"KAFKA_VALUE_SERIALIZER"
				));

	}

	public static void main(String[] args)  {
		registerRequiredEnvVars();

		// Check if device resolver should be mocked
		if (System.getenv("MOCK_DEVICE_RESOLVER").equals("true")){
			logger.debug("Mocking device resolver");
			Device mockDevice = new Device();
			mockDevice.setModelNumber(System.getenv("MOCK_DEVICE_MODEL_NUMBER"));
			DeviceResolver mockDeviceResolver = mock(DeviceResolver.class);
			FHIR2Milou.deviceResolver = mockDeviceResolver;
			when(mockDeviceResolver.findDeviceFromCache(anyString())).thenReturn(mockDevice);
		}

		// Check if patient resolver should be mocked
		if (System.getenv("MOCK_PATIENT_RESOLVER").equals("true")){
			logger.debug("Mocking patient resolver");
			Patient mockPatient = new Patient();
			mockPatient.addName().addGiven(System.getenv("MOCK_PATIENT_LAST_NAME")).setFamily(System.getenv("MOCK_PATIENT_FIRST_NAME"));
			mockPatient.addIdentifier(new Identifier().setSystem("urn:oid:1.2.208.176.1.2").setValue(System.getenv("MOCK_PATIENT_CPR")));
			mockPatient.setId("http://example.com/patient_service/1");

			PatientResolver mockResolver = mock(PatientResolver.class);
			FHIR2Milou.patientResolver = mockResolver;
			when(mockResolver.findPatientInCache(anyString())).thenReturn(mockPatient);
			when(mockResolver.findPatientInCache(null)).thenReturn(mockPatient);
		}

		if (topics == null){
			topics = ServiceUtils.getTopics();
		}

		logger.debug("Starting FHIR2Milou service {}", kv("topic", topics.isEmpty() ? "" : topics.get(0)));
		try {
			if (kafkaConsumeAndProcessThread == null){
				KafkaEventProducer kafkaEventProducer = new KafkaEventProducer("FHIR2Milou service");
				EventProcessor eventProcessor = new MyEventProcessor();
				KafkaConsumeAndProcess kafkaConsumeAndProcess = new KafkaConsumeAndProcess(topics, kafkaEventProducer, eventProcessor);
				kafkaConsumeAndProcess.registerInterceptor(new MetricsEventConsumerInterceptorAdaptor());
				if (Env.isSetToTrue("ENABLE_DIAS_AUTHENTICATION")) {
					DiasUserContextResolver contextResolver = new DiasUserContextResolver(System.getenv("USER_CONTEXT_SERVICE_URL"));
					kafkaConsumeAndProcess.registerInterceptor(new DiasEventConsumerInterceptor());
				}
				kafkaConsumeAndProcessThread = kafkaConsumeAndProcess;
			}

			new Thread(kafkaConsumeAndProcessThread, "KafkaProcessThread").start();
		} catch (KafkaInitializationException | MessagingInitializationException e) {
			logger.error("Error during Kafka initialization: ", e);
			System.exit(1);
		}

	}

}
