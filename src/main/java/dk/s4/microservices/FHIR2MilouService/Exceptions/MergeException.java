package dk.s4.microservices.FHIR2MilouService.Exceptions;

public class MergeException  extends Exception {

    public MergeException(String message) {
        super(message);
    }


}

