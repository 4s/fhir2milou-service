package dk.s4.microservices.FHIR2MilouService.Exceptions;

public class ValidationException extends Exception{
    public ValidationException(String msg){
        super(msg);
    }
}
