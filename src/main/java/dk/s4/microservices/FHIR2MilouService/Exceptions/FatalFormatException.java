package dk.s4.microservices.FHIR2MilouService.Exceptions;

public class FatalFormatException extends Exception{
    public FatalFormatException(String s){
        super(s);
    }
}
