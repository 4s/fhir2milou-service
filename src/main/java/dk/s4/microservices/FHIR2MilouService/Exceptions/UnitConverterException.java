package dk.s4.microservices.FHIR2MilouService.Exceptions;

public class UnitConverterException extends Exception {

    public UnitConverterException(String message){
        super(message);
    }

}
