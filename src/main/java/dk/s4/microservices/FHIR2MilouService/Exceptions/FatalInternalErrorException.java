package dk.s4.microservices.FHIR2MilouService.Exceptions;

public class FatalInternalErrorException extends Exception{
    public FatalInternalErrorException(String s){
        super(s);
    }
}
