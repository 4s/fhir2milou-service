package dk.s4.microservices.FHIR2MilouService.Exceptions;

public class SampleDataException extends Exception {

    public SampleDataException(String msg){
        super(msg);
    }

}
