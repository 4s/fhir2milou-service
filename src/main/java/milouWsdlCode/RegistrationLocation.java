
package milouWsdlCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegistrationLocation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegistrationLocation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clinicField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.ImportCtg}RegistrationLocationClinic"/>
 *         &lt;element name="roomField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.ImportCtg}RegistrationLocationRoom"/>
 *         &lt;element name="wardField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.ImportCtg}RegistrationLocationWard"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationLocation", propOrder = {
    "clinicField",
    "roomField",
    "wardField"
})
public class RegistrationLocation {

    @XmlElement(required = true, nillable = true)
    protected RegistrationLocationClinic clinicField;
    @XmlElement(required = true, nillable = true)
    protected RegistrationLocationRoom roomField;
    @XmlElement(required = true, nillable = true)
    protected RegistrationLocationWard wardField;

    /**
     * Gets the value of the clinicField property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationLocationClinic }
     *     
     */
    public RegistrationLocationClinic getClinicField() {
        return clinicField;
    }

    /**
     * Sets the value of the clinicField property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationLocationClinic }
     *     
     */
    public void setClinicField(RegistrationLocationClinic value) {
        this.clinicField = value;
    }

    /**
     * Gets the value of the roomField property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationLocationRoom }
     *     
     */
    public RegistrationLocationRoom getRoomField() {
        return roomField;
    }

    /**
     * Sets the value of the roomField property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationLocationRoom }
     *     
     */
    public void setRoomField(RegistrationLocationRoom value) {
        this.roomField = value;
    }

    /**
     * Gets the value of the wardField property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationLocationWard }
     *     
     */
    public RegistrationLocationWard getWardField() {
        return wardField;
    }

    /**
     * Sets the value of the wardField property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationLocationWard }
     *     
     */
    public void setWardField(RegistrationLocationWard value) {
        this.wardField = value;
    }

}
