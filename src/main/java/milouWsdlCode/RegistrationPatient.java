
package milouWsdlCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegistrationPatient complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegistrationPatient">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nameField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.ImportCtg}RegistrationPatientName"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationPatient", propOrder = {
    "idField",
    "nameField"
})
public class RegistrationPatient {

    @XmlElement(required = true, nillable = true)
    protected String idField;
    @XmlElement(required = true, nillable = true)
    protected RegistrationPatientName nameField;

    /**
     * Gets the value of the idField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdField() {
        return idField;
    }

    /**
     * Sets the value of the idField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdField(String value) {
        this.idField = value;
    }

    /**
     * Gets the value of the nameField property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationPatientName }
     *     
     */
    public RegistrationPatientName getNameField() {
        return nameField;
    }

    /**
     * Sets the value of the nameField property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationPatientName }
     *     
     */
    public void setNameField(RegistrationPatientName value) {
        this.nameField = value;
    }

}
