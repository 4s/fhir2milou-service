
package milouWsdlCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Registration complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Registration">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contactField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.ImportCtg}RegistrationContact"/>
 *         &lt;element name="ctgField" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="deviceIDField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="endTimeField" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="locationField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.ImportCtg}RegistrationLocation"/>
 *         &lt;element name="markersField" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfdateTime"/>
 *         &lt;element name="patientField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.ImportCtg}RegistrationPatient"/>
 *         &lt;element name="startTimeField" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="tocoShiftField" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Registration", propOrder = {
    "contactField",
    "ctgField",
    "deviceIDField",
    "endTimeField",
    "locationField",
    "markersField",
    "patientField",
    "startTimeField",
    "tocoShiftField"
})
public class Registration {

    @XmlElement(required = true, nillable = true)
    protected RegistrationContact contactField;
    @XmlElement(required = true, nillable = true)
    protected byte[] ctgField;
    @XmlElement(required = true, nillable = true)
    protected String deviceIDField;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endTimeField;
    @XmlElement(required = true, nillable = true)
    protected RegistrationLocation locationField;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfdateTime markersField;
    @XmlElement(required = true, nillable = true)
    protected RegistrationPatient patientField;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startTimeField;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer tocoShiftField;

    /**
     * Gets the value of the contactField property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationContact }
     *     
     */
    public RegistrationContact getContactField() {
        return contactField;
    }

    /**
     * Sets the value of the contactField property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationContact }
     *     
     */
    public void setContactField(RegistrationContact value) {
        this.contactField = value;
    }

    /**
     * Gets the value of the ctgField property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getCtgField() {
        return ctgField;
    }

    /**
     * Sets the value of the ctgField property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setCtgField(byte[] value) {
        this.ctgField = value;
    }

    /**
     * Gets the value of the deviceIDField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceIDField() {
        return deviceIDField;
    }

    /**
     * Sets the value of the deviceIDField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceIDField(String value) {
        this.deviceIDField = value;
    }

    /**
     * Gets the value of the endTimeField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndTimeField() {
        return endTimeField;
    }

    /**
     * Sets the value of the endTimeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndTimeField(XMLGregorianCalendar value) {
        this.endTimeField = value;
    }

    /**
     * Gets the value of the locationField property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationLocation }
     *     
     */
    public RegistrationLocation getLocationField() {
        return locationField;
    }

    /**
     * Sets the value of the locationField property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationLocation }
     *     
     */
    public void setLocationField(RegistrationLocation value) {
        this.locationField = value;
    }

    /**
     * Gets the value of the markersField property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfdateTime }
     *     
     */
    public ArrayOfdateTime getMarkersField() {
        return markersField;
    }

    /**
     * Sets the value of the markersField property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfdateTime }
     *     
     */
    public void setMarkersField(ArrayOfdateTime value) {
        this.markersField = value;
    }

    /**
     * Gets the value of the patientField property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationPatient }
     *     
     */
    public RegistrationPatient getPatientField() {
        return patientField;
    }

    /**
     * Sets the value of the patientField property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationPatient }
     *     
     */
    public void setPatientField(RegistrationPatient value) {
        this.patientField = value;
    }

    /**
     * Gets the value of the startTimeField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartTimeField() {
        return startTimeField;
    }

    /**
     * Sets the value of the startTimeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartTimeField(XMLGregorianCalendar value) {
        this.startTimeField = value;
    }

    /**
     * Gets the value of the tocoShiftField property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTocoShiftField() {
        return tocoShiftField;
    }

    /**
     * Sets the value of the tocoShiftField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTocoShiftField(Integer value) {
        this.tocoShiftField = value;
    }

}
