
package milouWsdlCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegistrationPatientName complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegistrationPatientName">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="firstField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lastField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="middleField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationPatientName", propOrder = {
    "firstField",
    "lastField",
    "middleField"
})
public class RegistrationPatientName {

    @XmlElement(required = true, nillable = true)
    protected String firstField;
    @XmlElement(required = true, nillable = true)
    protected String lastField;
    @XmlElement(required = true, nillable = true)
    protected String middleField;

    /**
     * Gets the value of the firstField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstField() {
        return firstField;
    }

    /**
     * Sets the value of the firstField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstField(String value) {
        this.firstField = value;
    }

    /**
     * Gets the value of the lastField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastField() {
        return lastField;
    }

    /**
     * Sets the value of the lastField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastField(String value) {
        this.lastField = value;
    }

    /**
     * Gets the value of the middleField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleField() {
        return middleField;
    }

    /**
     * Sets the value of the middleField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleField(String value) {
        this.middleField = value;
    }

}
