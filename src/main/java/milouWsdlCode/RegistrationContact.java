
package milouWsdlCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RegistrationContact complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegistrationContact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="gestationField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.ImportCtg}RegistrationContactGestation"/>
 *         &lt;element name="parityField" type="{http://schemas.datacontract.org/2004/07/Milou.Server.ImportCtg}RegistrationContactParity"/>
 *         &lt;element name="timeField" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationContact", propOrder = {
    "gestationField",
    "parityField",
    "timeField"
})
public class RegistrationContact {

    @XmlElement(required = true, nillable = true)
    protected RegistrationContactGestation gestationField;
    @XmlElement(required = true, nillable = true)
    protected RegistrationContactParity parityField;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeField;

    /**
     * Gets the value of the gestationField property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationContactGestation }
     *     
     */
    public RegistrationContactGestation getGestationField() {
        return gestationField;
    }

    /**
     * Sets the value of the gestationField property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationContactGestation }
     *     
     */
    public void setGestationField(RegistrationContactGestation value) {
        this.gestationField = value;
    }

    /**
     * Gets the value of the parityField property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationContactParity }
     *     
     */
    public RegistrationContactParity getParityField() {
        return parityField;
    }

    /**
     * Sets the value of the parityField property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationContactParity }
     *     
     */
    public void setParityField(RegistrationContactParity value) {
        this.parityField = value;
    }

    /**
     * Gets the value of the timeField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeField() {
        return timeField;
    }

    /**
     * Sets the value of the timeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeField(XMLGregorianCalendar value) {
        this.timeField = value;
    }

}
