
package milouWsdlCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegistrationContactGestation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegistrationContactGestation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="totalDaysField" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationContactGestation", propOrder = {
    "totalDaysField"
})
public class RegistrationContactGestation {

    protected int totalDaysField;

    /**
     * Gets the value of the totalDaysField property.
     * 
     */
    public int getTotalDaysField() {
        return totalDaysField;
    }

    /**
     * Sets the value of the totalDaysField property.
     * 
     */
    public void setTotalDaysField(int value) {
        this.totalDaysField = value;
    }

}
