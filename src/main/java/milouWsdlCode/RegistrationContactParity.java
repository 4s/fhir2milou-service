
package milouWsdlCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegistrationContactParity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegistrationContactParity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="gravidaField" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="paraField" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationContactParity", propOrder = {
    "gravidaField",
    "paraField"
})
public class RegistrationContactParity {

    protected int gravidaField;
    protected int paraField;

    /**
     * Gets the value of the gravidaField property.
     * 
     */
    public int getGravidaField() {
        return gravidaField;
    }

    /**
     * Sets the value of the gravidaField property.
     * 
     */
    public void setGravidaField(int value) {
        this.gravidaField = value;
    }

    /**
     * Gets the value of the paraField property.
     * 
     */
    public int getParaField() {
        return paraField;
    }

    /**
     * Sets the value of the paraField property.
     * 
     */
    public void setParaField(int value) {
        this.paraField = value;
    }

}
